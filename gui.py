from datetime import datetime
from PyQt5.QtCore import Qt, QThread, QObject,pyqtSignal, QMutex, QTimer
from PyQt5.QtGui import QIntValidator, QIcon, QPixmap
from PyQt5.QtWidgets import (QCheckBox, QWidget, QApplication, QVBoxLayout, QPushButton, QFileDialog, QMessageBox, QGridLayout,
                            QHBoxLayout, QLabel, QGroupBox,QComboBox, QLineEdit, QScrollArea, QProgressDialog, QProgressBar)

from serial_reader import env_driver, event
import matplotlib
matplotlib.use('Qt5Agg')

from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg, NavigationToolbar2QT as NavigationToolbar
from matplotlib.figure import Figure

import os.path


class MplCanvas(FigureCanvasQTAgg):
    def __init__(self, parent=None, width=5, height=4, dpi=100):
        fig = Figure(figsize=(width, height), dpi=dpi)
        self.axes = fig.add_subplot(111)
        super(MplCanvas, self).__init__(fig)



import time,json
auto_widget_dic = {}

def button_creator(name, action, set_width = -1, unique_name = None):
    button = QPushButton(name)
    button.clicked.connect(action)
    if set_width > 0:
        button.setFixedWidth( set_width )
    if unique_name:
        auto_widget_dic[unique_name] = button
    return button

def label_creator(name,set_width = -1, unique_name = None):
    label = QLabel(name)
    if set_width > 0:
        label.setFixedWidth( set_width )
    if unique_name:
        auto_widget_dic[unique_name] = label
    return label

def checkbox_creator(name,action = None, unique_name = None):
    cb = QCheckBox()
    cb.setChecked(1)
    cb.setObjectName(name)

    if action != None:
        cb.clicked.connect(action)

    if unique_name:
        auto_widget_dic[unique_name] = cb
    return cb

def valbox_creator(default = None, set_width = -1, val_min = None, val_max = None, unique_name = None):
    if default:
        vb = QLineEdit(f"{default}")
    else:
        vb = QLineEdit("")

    if set_width > 0:
        vb.setFixedWidth( set_width )

    if val_min != None and val_max != None:
        int_validator = QIntValidator()
        int_validator.setTop(val_max)
        int_validator.setBottom(val_min)
        vb.setValidator(int_validator)

    if unique_name:
        auto_widget_dic[unique_name] = vb

    return vb

def question_popup(text):
   msgBox = QMessageBox()
   msgBox.setIcon(QMessageBox.Information)
   msgBox.setText(text)
   msgBox.setWindowTitle("Alert")
   msgBox.setStandardButtons(QMessageBox.Ok | QMessageBox.Cancel)
   #msgBox.buttonClicked.connect(msgButtonClick)

   returnValue = msgBox.exec()
   if returnValue == QMessageBox.Ok:
       return 1
   else:
       return 0

def convert(meas_name,value,type = "float",keep_units = True):
    if meas_name == "timestamp":
        if type == "str":
            return datetime.fromtimestamp(value).strftime("%Y:%m:%d :%H:%M:%S")
        else:
            return value
    elif "_T" in meas_name or "PT_" in meas_name:
        val = round(value*0.1-50,1)
        if type == "str":
            return f"{val:.1f}"+ keep_units*" °"
        else:
            return val
    elif "_H" in meas_name:
        val = round(value*0.1,1)
        if type == "str":
            return f"{val:.1f}"+keep_units*" %"
        else:
            return val
    elif "acc" in meas_name:
        val = round(value/30.,1)
        if type == "str":
            return f"{val:.1f}"
        else:
            return val
    else:
        if type == "str":
            return f"{value}"
        else:
            return value



cmd_list = []
cmd_list_mutex = QMutex()

event_buffer = []
event_buffer_mutex = QMutex()

flash_storage_files = []
flash_storage_files_mutex = QMutex()





class gui(QObject):
    global cmd_list
    command   = pyqtSignal(str)

    def __init__(self):
        self.conf = json.load(open("conf.json","r"))
        
        super(gui, self).__init__()
        self.config_worker()
        self.break_id = 0
        self.app = QApplication([])

        self.event_list = []
        self.to_plot = []
        self.data = None
        self.data_zs = None
        self.data_avg = None
        self.data_avg_buff = None
        self.last_autosync =0


        window = QWidget()
        window.setGeometry(0, 0, 1200, 800)
        window.setWindowTitle("Magnet test environment monitor")

        side_buttons_group = QGroupBox()
        side_buttons = QGridLayout(side_buttons_group)
        side_buttons.setAlignment(Qt.AlignTop)
        side_buttons.addWidget(button_creator("Download from flash" ,self.download_all_popup, 200))
        self.flash_progress_bar = QProgressBar()
        self.flash_progress_bar.setValue(0)
        self.flash_progress_bar.setDisabled(1)

        side_buttons.addWidget(self.flash_progress_bar)

        side_buttons.addWidget(button_creator("Sync timing"         ,self.sync, 200))
        side_buttons.addWidget(label_creator("Sync status :",200))
        self.UI_loop_label = label_creator("No data...",200)
        side_buttons.addWidget(self.UI_loop_label)
        side_buttons.addWidget(label_creator("Runner status :",200))
        self.runner_label = label_creator("Not initialised...",200)
        side_buttons.addWidget(self.runner_label)
        side_buttons.addWidget(QLabel("Plot mode :"))
        self.plot_selector = QComboBox()
        self.plot_selector.addItems(["All","Zero-suppressed", "Averaged"])
        side_buttons.addWidget(self.plot_selector)
        side_buttons.addWidget(button_creator("Purge plots", self.purge,200))
        self.data_status = QLabel("")
        side_buttons.addWidget(self.data_status)
        side_buttons.addWidget(button_creator("Reset axis",self.reset_axis,200))
        side_buttons.addWidget(button_creator("Reset I2C" ,lambda x: cmd_list.append("reset_I2C"),200))
        #side_buttons.addWidget(button_creator("DCS config" ,self.config_popup,200))



        main_box_group      = QGroupBox()
        main_box_widgets    = QVBoxLayout(main_box_group)
        title = QLabel("Live measurements")
        title.setFixedHeight(50)
        title.setFixedWidth(1000)
        title.setAlignment(Qt.AlignCenter)
        main_box_widgets.addWidget(title,0)

        live_group   = QGroupBox()
        live_widgets = QGridLayout(live_group)

        self.live_labels = []
        for meas in self.conf["meas_to_show"]:
            if meas in self.conf["alias"]:
                self.live_labels.append(QLabel(self.conf["alias"][meas]))
            else:
                self.live_labels.append(QLabel(meas))
        self.live_values = [ QLabel("None") for meas in self.live_labels]
        self.live_checkboxes = [checkbox_creator(meas,self.refresh_checkboxes) for meas in self.conf["meas_to_show"]]

        n_cols = 6
        n_widgets_per_col = int(len(self.live_labels) / n_cols) + 1
        row_offset = 2
        for index,live_label in enumerate(self.live_labels):
            col = 3*int(index/n_widgets_per_col)
            row = index%n_widgets_per_col + row_offset
            live_widgets.addWidget(self.live_checkboxes[index],row,col)
            live_widgets.addWidget(live_label,row,col+1)
            live_widgets.addWidget(self.live_values[index],row,col+2)

        live_widgets.addWidget(button_creator("Remove all temp",self.selector("T",0)),n_widgets_per_col+row_offset,1)
        live_widgets.addWidget(button_creator("Select all temp",self.selector("T",1)),n_widgets_per_col+row_offset+1,1)
        live_widgets.addWidget(button_creator("Remove all hum",self.selector("H",0)),n_widgets_per_col+row_offset,4)
        live_widgets.addWidget(button_creator("Select all hum",self.selector("H",1)),n_widgets_per_col+row_offset+1,4)
        live_widgets.addWidget(button_creator("Remove all acc",self.selector("a",0)),n_widgets_per_col+row_offset,7)
        live_widgets.addWidget(button_creator("Select all acc",self.selector("a",1)),n_widgets_per_col+row_offset+1,7)


        graph_group  = QGroupBox()
        graph_widgets = QGridLayout(graph_group)
        self.plotCanvas = MplCanvas(graph_widgets, width=1000, height=600, dpi=100)
        self.reset_axis()
        self.should_refresh_legend = False

        self.plots = {}
        toolbar = NavigationToolbar(self.plotCanvas, graph_group)
        graph_widgets.addWidget(toolbar,0,0)
        graph_widgets.addWidget(self.plotCanvas,1,0)
        #graph_widgets.addWidget(button_creator("This is a button",self.hello_world),0,0)

        main_box_widgets.addWidget(live_group)
        main_box_widgets.addWidget(graph_group)


        main_layout = QHBoxLayout(window)
        main_layout.addWidget(side_buttons_group)
        main_layout.addWidget(main_box_group)

        self.timer=QTimer()
        self.timer.timeout.connect(self.refresh)
        self.timer.start(1000)

        self.refresh_checkboxes()
        self.start_worker()
        window.show()
        self.app.exec_()

    def selector(self,meas_type, status):
        def sel_fn():
            contains = []
            if meas_type == "T":
                contains = ["_T","PT"]
            elif meas_type == "H":
                contains = ["_H"]
            elif meas_type == "a":
                contains = ["acc"]

            for cb in self.live_checkboxes:
                for pattern in contains:
                    if pattern in cb.objectName():
                        cb.setChecked(status)
            self.refresh_checkboxes()
        return sel_fn

    def purge(self):
        self.data = None
        self.data_zs = None
        self.data_avg = None
        self.data_avg_buff = None


        """    int32_t timezone;
            int32_t refresh_period; // Refresh period (in seconds)
            int32_t flash_prescale; // Flash refresh period (in units of base refresh rate)
            int32_t dcs_thresh;     // 4 bits maj, 10 bits H_up, 10 bits H_down, 8 bits mask
            int32_t lemo_routing;   // 2 LSB = V_set signal
            int32_t misc;           // MSB to LSB : UNUSED"""


    def process(self,entry):
        fName = self.conf["save_file"]
        if not os.path.isfile(fName):
            with open(fName,"w") as f:
                header = ",".join(entry.keys())+"\n"
                header_2 = ",".join([self.conf["alias"][key] if key in self.conf["alias"].keys() else key for key in entry.keys()])+"\n"
                f.write(header+header_2)
        with open(fName,"a") as f:
            line = ",".join([convert(meas,entry[meas],"str",keep_units=False) for meas in entry.keys()])+"\n"
            f.write(line)

        self.event_list.append(entry)
        if self.data == None:
            self.data = {}
            self.data_zs = {}
            self.data_avg_buff = {}
            self.data_avg = {}
            ttt = entry["timestamp"]
            for meas in entry.keys():
                xxx = convert(meas,entry[meas],"float")
                self.data[meas] = [xxx]
                self.data_zs[meas] = [(xxx,ttt)]
                self.data_avg_buff[meas] = [xxx]
                self.data_avg[meas] = []
        else:
            ttt = entry["timestamp"]
            max_zs_len = 0
            data_len = 0
            data_avg_len = 0
            for meas in entry.keys():
                xxx = convert(meas,entry[meas],"float")

                self.data[meas].append(xxx)

                if data_len == 0:
                    data_len = len(self.data[meas])

                if len(self.data[meas]) > self.conf["max_data"]:
                    self.data[meas] = self.data[meas][60:]

                if len(self.data_zs[meas]) > max_zs_len:
                    max_zs_len = len(self.data_zs[meas])

                if meas in self.conf["meas_to_show"] and xxx != self.data_zs[meas][-1][0]:
                    self.data_zs[meas].append((xxx,ttt))
                    if len(self.data_zs[meas]) > self.conf["max_data"]:
                        self.data_zs[meas] = self.data_zs[meas][60:]

                self.data_avg_buff[meas].append(xxx)
                if len(self.data_avg_buff[meas]) > 180:
                    avg = sum(self.data_avg_buff[meas][0:60])*1./60
                    self.data_avg[meas].append(avg)
                    if len(self.data_avg[meas]) > self.conf["max_data"]:
                        self.data_avg[meas] = self.data_avg[meas][60:]
                    self.data_avg_buff[meas] = self.data_avg_buff[meas][60:]
                if data_avg_len == 0:
                    data_avg_len = len(self.data_avg[meas])
            self.data_status.setText(f"Data used : {data_len}, {data_avg_len}, {max_zs_len} / {self.conf['max_data']}")

    def reset_axis(self):
        self.plotCanvas.axes.set(xlim = [datetime.fromtimestamp(time.time()-10),datetime.fromtimestamp(time.time()+3600)],ylim = [-60,50])

    def refresh(self):
        global event_buffer
        event_buffer_mutex.lock()

        if len(event_buffer) > 0:
            for entry in event_buffer:
                if entry != 0:
                    self.process(entry)
            event_buffer = []
            event_buffer_mutex.unlock()
            for index,meas in enumerate(self.conf["meas_to_show"]):
                meas_value = convert(meas,self.event_list[-1][meas])
                ovt = 1
                if meas in self.conf["meas_limits_override"].keys():
                    ovt = meas_value < self.conf["meas_limits_override"][meas][0] or meas_value > self.conf["meas_limits_override"][meas][1]
                else:
                    meas_type  = ""
                    if "_T" in meas or "PT_" in meas:
                        meas_type = "temp"
                    elif "_H" in meas:
                        meas_type = "hum"
                    else:
                        meas_type = "acc"

                    ovt = meas_value < self.conf["meas_limits"][meas_type][0] or meas_value > self.conf["meas_limits"][meas_type][1]

                was_ovt = "red" in self.live_values[index].styleSheet()
                self.live_values[index].setText(convert(meas,self.event_list[-1][meas],"str"))
                if ovt and not was_ovt:
                    self.live_values[index].setStyleSheet("QLabel { background-color : red; color : blue; }")
                elif not ovt and was_ovt:
                    self.live_values[index].setStyleSheet("QLabel { color : black; }")
        else:
            event_buffer_mutex.unlock()

        if len(self.event_list) > 0:
            if self.event_list[-1]["timestamp"] != self.last_autosync and abs(time.time()-self.event_list[-1]["timestamp"]) > self.conf["autosync"] :
                self.last_autosync = self.event_list[-1]["timestamp"]
                print("Autosync")
                self.sync()

            if abs(time.time()-self.event_list[-1]["timestamp"]) > 5:
                self.UI_loop_label.setText(f"LOST SYNC ! dt = {int(time.time()-self.event_list[-1]['timestamp'])}s")
                self.UI_loop_label.setStyleSheet("QLabel { background-color : red; color : blue; }")
            else:
                self.UI_loop_label.setText(f"dt = {int(time.time()-self.event_list[-1]['timestamp'])}s")
                self.UI_loop_label.setStyleSheet("QLabel { }")

        # Now, processing plots...
        plot_type = self.plot_selector.currentText()
        for meas in self.conf["meas_to_show"]:
            if meas in self.to_plot:
                if meas in self.plots.keys():
                    if plot_type == "Zero-suppressed":
                        self.plots[meas].set_ydata([entry[0] for entry in self.data_zs[meas]]+[self.data[meas][-1]])
                        self.plots[meas].set_xdata([datetime.fromtimestamp(entry[1]) for entry in self.data_zs[meas]] + [datetime.fromtimestamp(self.data["timestamp"][-1])])
                    elif plot_type == "Averaged":
                        self.plots[meas].set_ydata(self.data_avg[meas] + self.data[meas][-len(self.data_avg_buff[meas]):])
                        self.plots[meas].set_xdata([datetime.fromtimestamp(t) for t in self.data_avg["timestamp"]] + [datetime.fromtimestamp(t) for t in self.data["timestamp"][-len(self.data_avg_buff["timestamp"]):]])
                    else:
                        self.plots[meas].set_ydata(self.data[meas])
                        self.plots[meas].set_xdata([datetime.fromtimestamp(t) for t in self.data["timestamp"]])
                else:
                    meas_name = meas
                    if meas in self.conf["alias"]:
                        meas_name = self.conf["alias"][meas]
                    if self.data != None:
                        self.plots[meas] =  self.plotCanvas.axes.plot_date([datetime.fromtimestamp(t) for t in self.data["timestamp"]], self.data[meas],label=meas_name,linestyle='dashed', linewidth=1, markersize=4)[0]

        if self.should_refresh_legend == True:
            self.plotCanvas.axes.legend(loc='upper right')
            self.should_refresh_legend = False

        self.plotCanvas.draw()
        self.plotCanvas.flush_events()

    def refresh_checkboxes(self):
        self.to_plot = [cb.objectName() for cb in self.live_checkboxes if cb.isChecked()]
        to_remove = []
        for meas in self.plots.keys():
            if meas not in self.to_plot:
                to_remove.append(meas)
        for meas in to_remove:
            self.plots[meas].remove()
            del self.plots[meas]
        self.should_refresh_legend = True

    def download_all_popup(self):
        flash_storage_files_mutex.lock()
        global flash_storage_files
        if len(flash_storage_files) > 0:
            self.alert("Error, previous transfer not done yet !")
            flash_storage_files_mutex.unlock()
            return
        flash_storage_files_mutex.unlock()
        dlg = QFileDialog()
        dlg.setFileMode(QFileDialog.AnyFile)
        dlg.setNameFilter("bin (*.bin)")
        dlg.setAcceptMode(QFileDialog.AcceptSave)

        if dlg.exec_():
            filenames = dlg.selectedFiles()
            if len(filenames) < 1:
                self.alert("Error, no file specified")
                return
            binFileName = filenames[0]
            csvFileName = binFileName[:-4]+".csv"
            if os.path.isfile(csvFileName):
                if question_popup(f"This will overwrite the file {csvFileName}.\nAre you sure?") == 0:
                    return

            flash_storage_files_mutex.lock()
            flash_storage_files = [binFileName]
            flash_storage_files_mutex.unlock()

            cmd_list_mutex.lock()
            global cmd_list
            cmd_list.append("download_flash")
            cmd_list_mutex.unlock()

    def sync(self):
        cmd_list_mutex.lock()
        cmd_list.append("sync")
        cmd_list_mutex.unlock()


    def alert(self,txt):
        msgBox=QMessageBox()
        msgBox.setText(txt)
        msgBox.exec()

    def hello_world(self):
        self.alert("Hello")

    def get_event(self):
        #self.alert(self.driver.read_event().__repr__())
        self.alert("Not implemented")

    def update_flash_progress(self,value):
        if value >= 0:
            self.flash_progress_bar.setValue(value)
            self.flash_progress_bar.setEnabled(1)
        else:
            self.flash_progress_bar.setValue(0)
            self.flash_progress_bar.setDisabled(1)

    def update_runner_status(self,status):
        if status == 1:
            self.runner_label.setText("Busy...")
            self.runner_label.setStyleSheet("QLabel { }")
        elif status == 0:
            self.runner_label.setText("Running !")
            self.runner_label.setStyleSheet("QLabel { color : green; }")
        else :
            self.runner_label.setText("ERROR !")
            self.runner_label.setStyleSheet("QLabel { background-color : red; color : blue; }")

    def config_worker(self):
        # Step 2: Create a QThread object
        self.thread = QThread()
        # Step 3: Create a worker object
        self.worker = serial_worker()
        self.worker.configure(self.conf)

        # Step 4: Move worker to the thread
        self.worker.moveToThread(self.thread)
        # Step 5: Connect signals and slots
        self.thread.started.connect(self.worker.main_loop)
        self.worker.busy.connect(self.update_runner_status)
        self.worker.flash_progress.connect(self.update_flash_progress)
        #print(self.command)
        self.command.connect(self.worker.set_cmd)
        #self.worker.finished.connect(self.thread.quit)
        #self.worker.finished.connect(self.worker.deleteLater)
        #self.thread.finished.connect(self.thread.deleteLater)
        self.worker.running.connect(self.print_running)
        # Step 6: Start the thread
    def start_worker(self):
        self.thread.start()

    def print_running(self,tt):
        if abs(tt-time.time()) < 2:
            print("Running !")
        else:
            print("STUCK !")

class serial_worker(QObject):
    running        = pyqtSignal(int)
    new_event      = pyqtSignal(event)
    busy           = pyqtSignal(int)
    flash_progress = pyqtSignal(int)
    flash_downloading = False

    def configure(self,config):
        self.driver = env_driver(config)
        self.conf = config

    def set_cmd(self,cmd):
        self.cmd.append(cmd)

    def start_flash_download(self):
        self.block_id = 0
        self.n_blocks = self.driver.get_n_flash_blocks()
        self.flash_downloading = True
        flash_storage_files_mutex.lock()
        self.data = bytes()
        if len(flash_storage_files) == 0:
            print("Error, no file to save to !")
            self.stop_flash_download()

        self.binary_file = flash_storage_files[0]
        flash_storage_files_mutex.unlock()

    def get_flash_block(self):
        data = self.driver.read_flash(self.block_id)
        if data != None and len(data) > 0 and data != [0]:
            self.data += data
            if len(self.data) > 512*1024:
                with open(self.binary_file,"ab") as f:
                    f.write(self.data)
                self.data = bytes()

            self.block_id +=1
            if self.block_id >= self.n_blocks:
                self.stop_flash_download()
            else:
                self.flash_progress.emit(int(100*self.block_id/self.n_blocks))
        else:
            print("Warning, unable to read data !")


    def stop_flash_download(self):
        self.flash_downloading = False
        with open(self.binary_file,"ab") as f:
            f.write(self.data)

        global flash_storage_files
        flash_storage_files_mutex.lock()
        flash_storage_files = []
        flash_storage_files_mutex.unlock()
        self.flash_progress.emit(-1)

    def main_loop(self):
        global cmd_list

        self.cmd = ""
        while(True):
            cmd_list_mutex.lock()
            to_process = [ccc for ccc in cmd_list]
            cmd_list = []
            cmd_list_mutex.unlock()


            if to_process != []:
                self.busy.emit(1)
                for ccc in to_process:
                    if ccc == "sync":
                        print("Syncinc !")
                        self.driver.sync()
                    elif ccc == "download_flash":
                        self.start_flash_download()
                    elif ccc =="reset_I2C":
                        self.driver.reset_I2C()
                    else:
                        print(f"Ignoring command : {ccc}")
                self.busy.emit(0)

            n_evt = self.driver.read_buffer()
            if n_evt == None:
                n_evt = -1
            if n_evt > 0:
                global event_buffer
                event_buffer_mutex.lock()
                self.busy.emit(1)
                for i in range(n_evt):
                    event_buffer.append(self.driver.read_event())
                event_buffer_mutex.unlock()
                self.busy.emit(0)
            elif n_evt < 0:
                self.busy.emit(-1)
                self.driver.reconnect()


            if self.flash_downloading == 1:
                previous_loop = time.time()
                while (time.time() - previous_loop < 1) and self.flash_downloading == True:
                    self.get_flash_block()
            else:
                time.sleep(1)

