import json, time, os
from serial_reader import env_driver
from gui import convert

def batch_runner(config_file = "conf.json"):
    conf = json.load(open(config_file,"r"))
    fName = conf["save_file"]

    driver = env_driver(config_file)
    event_buffer = []
    while(True):
        n_evt = driver.read_buffer()
        if n_evt == None:
            n_evt = -1
        if n_evt > 0:
            for i in range(n_evt):
                entry = driver.read_event()
                if not os.path.isfile(fName):
                    #Creating csv file if it doesn't exist
                    with open(fName,"w") as f:
                        header = ",".join(entry.keys())+"\n"
                        header_2 = ",".join([conf["alias"][key] if key in conf["alias"].keys() else key for key in entry.keys()])+"\n"
                        f.write(header+header_2)

                with open(fName,"a") as f:
                    line = ",".join([convert(meas,entry[meas],"str",keep_units=False) for meas in entry.keys()])+"\n"
                    f.write(line)
                

                to_print = convert("timestamp",entry["timestamp"],"str",keep_units=False)
                for meas in conf["meas_to_show"]:
                    to_print+=f"\t {conf['alias'][meas] if meas in conf['alias'].keys() else meas} {convert(meas,entry[meas],'str')}"
                print(100*" ", end="\r")                   
                print(to_print,end="\r")

        else:
            time.sleep(1)


if __name__ == "__main__":
    batch_runner("conf_batch.json")




        
