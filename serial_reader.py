import json, glob, serial, traceback, datetime, time, sys
from os import CLD_DUMPED
from struct import pack

LOG_PRINT   = (-1,"    ")
LOG_ERROR   = (0 ,"E : ")
LOG_WARNING = (1 ,"W : ")
LOG_DEBUG   = (2 ,"D : ")
LOG_ALL     = (3 ,"    ")


class env_driver:
    def __init__(self,config = "conf.json"):
        if type(config) == str:
            self.load_conf(config)
        elif type(config) == dict:
            self.conf = config
        else:
            self.log(f"Error, type of config ({type(config)}) not supported",LOG_ERROR)
        self.connected = False

    def log(self,message,level=LOG_ALL):
        if self.conf["verbose"] >= level[0]:
            to_log = datetime.datetime.now().strftime("%Y:%m:%d_%H:%M:%S")+" "+level[1]+message
            print(to_log)
            with open(self.conf["log_file"],"a") as f:
                f.write(to_log+"\n")

    def load_conf(self,config_file):
        self.conf = json.load(open(config_file,"r"))


    def print_conf(self):
        self.log("Conf : ",LOG_PRINT)
        for key in self.conf:
            self.log(f"{key} {(15-len(key))*' '} {self.conf[key]}",LOG_PRINT)

    def connect(self):
        self.ser = None
        try:
            ports = glob.glob(self.conf["port"])
            if len(ports) == 0:
                self.log("No port available",LOG_ERROR)
                return 0
            self.log(f"Found {len(ports)} ports : {ports}")
            #elif len(ports) > 1:
            #    self.log(f"Warning, {len(ports)} possible connections found...",LOG_WARNING)
            #    self.log(ports,LOG_WARNING)
            #    self.log(f"Trying {ports[0]}",LOG_WARNING)
            for port in ports:
                self.log(f"Trying to connect on port {port}",LOG_DEBUG)
                self.ser = serial.Serial(port, 9600, timeout=2, xonxoff=False, rtscts=False, dsrdtr=False)
                self.ser.flushInput()
                self.ser.flushOutput()
                self.connected=True
                if self.reconnect():
                    return 1
                else:
                    self.log(f"Unable to ping Rh/T on port {port}", LOG_DEBUG)
            self.connected = False
            self.log("Unable to ping Rh/T on any port", LOG_ERROR)
        except Exception as e:
            traceback.print_exc()
            self.log(str(e),LOG_ERROR)
            self.connected = False
            return 0
    
    

    def get_acknowledge(self):
        rc = self.read(1)
        return (rc != None and len(rc) > 0 and rc[0] == self.conf["cmd"]["acknowledge"])

    def send(self,data, ask_aknowledge = False):
        if not self.connected == True:
            if not self.connect():
                self.log("Error, unable to connect !", LOG_ERROR)
                return 0
        try:
            self.ser.write(bytearray(data))
            if ask_aknowledge and not self.get_acknowledge():
                self.log("Error, No acknowledge received!", LOG_ERROR)
                self.connected = False
                return 0
            return 1
        except Exception as e:
            traceback.print_exc()
            self.connected = False
            self.log(str(e),LOG_ERROR)
            return 0

    def read(self,n_byte):
        if not self.connected == True:
            if not self.connect():
                self.log("Error, unable to connect !", LOG_ERROR)
                return None
        try:
            com_start = time.time()
            while self.ser.inWaiting() < n_byte:
                dt = time.time()-com_start
                #print(f"{dt} -- {self.ser.inWaiting()} bytes ready")
                if dt > self.conf["timeout"]:
                    self.log(f"Error, timeout while reading {n_byte} bytes (dt = {dt})",LOG_ERROR)
                    self.connected = False
                    return None

            data = self.ser.read(n_byte)
            if len(data) > n_byte:
                self.log(f"Warning, too much data received ! {len(data)}/{n_byte}", LOG_WARNING)
            return data
        except Exception as e:
            traceback.print_exc()
            self.connected = False
            self.log(str(e),LOG_ERROR)
            return []

    def send_cmd(self,cmd):
        if not cmd in self.conf["cmd"].keys():
            self.log(f"Error, unable to process cmd : {cmd}", LOG_ERROR)
            return 0
        return_code = self.send([self.conf["cmd"][cmd]],True)
        return return_code

    def read_buffer(self):
        if not self.send_cmd("read_buffer"):
            return -1
        else:
            dd = self.read(1)
            if dd != None and len(dd) > 0:
                return dd[0]
            else:
                return None

    def read_flash(self,package_id):
        if not self.send_cmd("read_flash"):
            return []
        else:
            self.send([package_id>>8,package_id&0xFF])
            return self.read(1024)

    def read_raw_event(self):
        if not self.send_cmd("read_event"):
            return []
        #Warning, change this !
        raw_data = self.read(48)
        return raw_data

    def get_n_flash_blocks(self):
        if not self.send_cmd("get_n_flash_blocks"):
            return 0
        xxx = self.read(2)
        n_b = (int(xxx[0])<<8)+int(xxx[1])
        return n_b

    def read_event(self):
        raw_data = self.read_raw_event()
        if raw_data == [] or raw_data == None:
            return 0
        if self.conf["dump_file"] != "":
            with open(self.conf["dump_file"],"ab") as f:
                f.write(raw_data)
        return event(self.conf["evt_def"][0],raw_data)

    def sync(self):
        self.send_cmd("sync")
        self.log("Sending resync",LOG_DEBUG)
        x = int(time.time())
        payload = [(x>>y)&0xFF for y in [24,16,8,0]]
        t = time.localtime()
        dt = int(time.mktime(time.struct_time([t.tm_year, t.tm_mon, t.tm_mday, t.tm_hour, t.tm_min, t.tm_sec, t.tm_wday, t.tm_yday, 0]))-time.mktime(time.gmtime())+0.5)
        payload += [dt>>y&0xFF for y in [24,16,8,0]]
        self.send(payload)

    def reset_I2C(self):
        return self.send_cmd("reset_I2C")

    def ping(self):
        return (self.send_cmd("ping"))

    def reconnect(self):
        if not self.connected == True:
            if not self.connect():
                self.log("Error, unable to connect !", LOG_ERROR)
                return 0
            return 1
        try:
            for i in range(20):
                print(f"{i+1}/20",end="\r")
                sys.stdout.flush()
                self.ser.write(bytearray([self.conf["cmd"]["ping"]]))

                com_start = time.time()
                data = []
                found_data = True
                while self.ser.inWaiting() < 1:
                    dt = time.time()-com_start
                    if dt > 0.01:
                        found_data = False
                        break
                if found_data:
                    data = self.ser.read(1)
                if len(data) > 0 and data[-1] == self.conf["cmd"]["acknowledge"]:
                    self.log(f"Connection succesfull after {i+1} tries !",LOG_DEBUG)
                    return 1
            return 0
        except Exception as e:
            traceback.print_exc()
            self.connected = False
            self.log(str(e),LOG_ERROR)
            return 0



class event:
    def __init__(self, struct, raw_data):
        self.struct = struct
        self.load(raw_data)

    def load(self,raw_data):
        self.data = {}
        if 8*len(raw_data) < self.struct["size"]:
            print(f"Error ! Input data too short ! {len(raw_data)} < {self.struct['size']}")
            return

        bit_string = ""
        for bb in range(47):
            xxx = bin(raw_data[bb])[2:]
            bit_string+=(8-len(xxx))*"0"+xxx
        self.data = {}

        bit_index = 0
        for entry_name,entry_size in self.struct["split"]:
            xxx = int(bit_string[bit_index:bit_index+entry_size],2)
            self.data[entry_name] = xxx
            bit_index+=entry_size

    def keys(self):
        return self.data.keys()

    def __getattr__(self, attr):
        return(self.data[attr])

    def __getitem__(self,key):
        return(self.data[key])

    def __repr__(self):
        ss = ""
        for key in self.data:
            ss+=f"{key} = {self.data[key]}  "
        return(ss)


if __name__ == "__main__":
    dd = env_driver()

    dd.read_event()
