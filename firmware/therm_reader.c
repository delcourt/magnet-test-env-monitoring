
#include "therm_reader.h"
#include "TCA9534.h"
#define HIH_ADDR 0x27
#define PCA9544_ADDR    (0b1110000) //MUX0


uint8_t mux_1 = 0;
uint8_t mux_2 = 0;
uint8_t mux_2_2 = 0;

uint16_t hih_status = 0;


int i2c_reader(int channel, uint8_t add, uint8_t * buff, int n_bytes){
    if (channel == 1){
        I2C_1_MasterClearStatus();
        I2C_1_MasterReadBuf(add, buff, n_bytes, I2C_1_MODE_COMPLETE_XFER);
        int breaker = 0;
        while(!(I2C_1_MasterStatus()&I2C_1_MSTAT_RD_CMPLT)){
            breaker++;
            if (breaker > 1000)
                return 0;
        }
        return !(I2C_1_MasterStatus() & I2C_1_MSTAT_ERR_ADDR_NAK);
    }    
    else if (channel == 2){
        I2C_2_MasterClearStatus();
        I2C_2_MasterReadBuf(add, buff, n_bytes, I2C_2_MODE_COMPLETE_XFER);
        int breaker = 0;
        while(!(I2C_2_MasterStatus()&I2C_2_MSTAT_RD_CMPLT)){
            breaker++;
            if (breaker > 1000)
                return 0;
        }
        return !(I2C_2_MasterStatus() & I2C_2_MSTAT_ERR_ADDR_NAK);
    }
    else
        return 0;
}


    
int i2c_writer_debug(int channel, uint8_t add, uint8_t * buff, int n_bytes, int * full_status){
    
    if (channel == 1){
        I2C_1_MasterClearStatus();
        I2C_1_MasterWriteBuf(add, buff, n_bytes, I2C_1_MODE_COMPLETE_XFER);   
        int breaker = 0;
        while(!(I2C_1_MasterStatus()&I2C_1_MSTAT_WR_CMPLT)){
            breaker++; 
            if (breaker > 100) 
                return 0;
        }        
        if (full_status != 0){
            *full_status = I2C_1_MasterStatus();
        }
        return !(I2C_1_MasterStatus()&I2C_1_MSTAT_ERR_ADDR_NAK);
    }
    else if (channel == 2){
        I2C_2_MasterClearStatus();
        I2C_2_MasterWriteBuf(add, buff, n_bytes, I2C_2_MODE_COMPLETE_XFER);   
        int breaker = 0;
        while(!(I2C_2_MasterStatus()&I2C_2_MSTAT_WR_CMPLT)){
            breaker++; 
            if (breaker > 1000) 
                return 0;
        }        
        return !(I2C_2_MasterStatus()&I2C_2_MSTAT_ERR_ADDR_NAK);
    }
    else{
        return 0;
    }
}

int i2c_writer(int channel, uint8_t add, uint8_t * buff, int n_bytes){
    return i2c_writer_debug(channel,add,buff,n_bytes,0);
}

void setup_HIH_I2C(){
    
    uint8_t MUX_buff[2];
    //uint8_t HIH_buff[5];
    //
    MUX_buff[0]=0b110;//activate all
    MUX_buff[1]=0;
    
    MUX_Set(3);
    CyDelay(100);
    
    I2C_1_Start();//mux 0
    I2C_2_Start();//mux 2
    
    
    
    I2C_1_MasterClearStatus();
    I2C_2_MasterClearStatus();
    //MUX_buff[0]=0b111;//activate all
    //I2C_buff[1]=mask&~TCA9534_DEFCONF;//ENx are active low, keeping them off. Also, leds off
    //I2C_1_MasterWriteBuf(PCA9544_ADDR, MUX_buff, 1, I2C_1_MODE_COMPLETE_XFER);
    
    
    // Looking for mux_1;
    for (int i = 0; i < 16; i++){
        uint8_t test_add = PCA9544_ADDR + i;
        if (i2c_reader(1,test_add,MUX_buff,0)){
            mux_1 = test_add;
            break;
        }
    }
    
    
    
    CyDelay(100);
    
    I2C_2_Start();//mux 2
    I2C_2_MasterClearStatus();
    for (int i = 0; i < 16; i++){
        uint8_t test_add = PCA9544_ADDR + i;
        if (i2c_reader(2,test_add,MUX_buff,0)){
            if (mux_2 == 0){
                mux_2 = test_add;
            }
            else{
                mux_2_2 = test_add;
                break;
            }
        }
    }


    //print_second_line()
    
    hih_status = 0;
    if (mux_1){
        I2C_1_MasterClearStatus();
        for (int i = 0; i < 4 ; i++){
            // Selecting det "i" on board 1
            MUX_buff[0] = 4+i;
            int status = i2c_writer(1,mux_1,MUX_buff,1);    // Select that line
            int full_status = 0;
            status    &= i2c_writer_debug(1,HIH_ADDR,MUX_buff,0, &full_status);  // Say hi to HIH
            //CyDelay(100);
            //status    &= i2c_reader(1,HIH_ADDR,HIH_buff,4);
            hih_status |= (status <<i);
        }
        
    }
    
    if (mux_2){
        I2C_2_MasterClearStatus();
        for (int i = 0; i < 4 ; i++){
            // Selecting det "i" on board 1
            MUX_buff[0] = 4+i;
            int status = i2c_writer(2,mux_2,MUX_buff,1);    // Select that line
            int full_status = 0;
            status    &= i2c_writer_debug(2,HIH_ADDR,MUX_buff,0,&full_status);  // Say hi to HIH
            //CyDelay(100);
            //status    &= i2c_reader(2,HIH_ADDR,HIH_buff,4);
            hih_status |= (status <<(i+4));
        }
        MUX_buff[0] = 0;
        i2c_writer(2,mux_2,MUX_buff,1);    // Turn off 
    }
    if (mux_2_2){
        I2C_2_MasterClearStatus();
        for (int i = 0; i < 4 ; i++){
            // Selecting det "i" on board 1
            MUX_buff[0] = 4+i;
            int status = i2c_writer(2,mux_2_2,MUX_buff,1);    // Select that line
            int full_status = 0;
            status    &= i2c_writer_debug(2,HIH_ADDR,MUX_buff,0,&full_status);  // Say hi to HIH
            //CyDelay(100);
            //status    &= i2c_reader(2,HIH_ADDR,HIH_buff,4);
            hih_status |= (status <<(i+8));
        }
        MUX_buff[0] = 0;
        i2c_writer(2,mux_2_2,MUX_buff,1);    // Turn off 
    }
    
    for (int i = 0; i < 8; i ++){
        if ((hih_status >> i )& 1){
            asm("nop");
        }
    }
    
    //MUX_Set(3);
}



int test_read(uint32_t * temp, uint32_t * hum){
    
    
    led_kprog_Write(!led_kprog_Read());
    
    if (hih_status == 0){
        return 0;
    }
    uint8_t HIH_buff[5];
    for (int i = 0; i<5; i++){
        HIH_buff[i] = 0;
    }
    uint8_t MUX_buff[2];
    int n_meas = 0;
    
    for (int i = 0; i<8; i ++){
        if ((hih_status >> i)&1){
            // Requiring data from sensor:
            int mux_id = (i>3) + 1;
            int mux_add = -1;
            if (mux_id == 1){
                MUX_Set(3);
                mux_add = mux_1;
            }
            else{
                MUX_Set(3);
                mux_add = mux_2;
            }
            
            MUX_buff[0] = 4+(i%4);
            
            int status = i2c_writer(mux_id,mux_add,MUX_buff,1);    // Select that line
            status    &= i2c_writer(mux_id,HIH_ADDR,MUX_buff,0);  // Say hi to HIH
        }
    }
    CyDelay(100);
    for (int i = 0; i<8; i ++){
        if ((hih_status >> i)&1){
            // Reading corresponding sensor :
            int mux_id = (i>3) + 1;
            int mux_add = -1;
            if (mux_id == 1){
                MUX_Set(3);
                mux_add = mux_1;
            }
            else{
                MUX_Set(3);
                mux_add = mux_2;
            }
            
            MUX_buff[0] = 4+(i%4);
            
            int status = i2c_writer(mux_id,mux_add,MUX_buff,1);    // Select that 
            status    &= i2c_reader(mux_id,HIH_ADDR,HIH_buff,4);
            
            float HIH_rh = 0;
            float HIH_tmp = -50;
            
            if (status){
                HIH_rh=100*(((HIH_buff[0]&0x3f)<<8)+HIH_buff[1]);
                HIH_rh/=(1<<14)-2;
                HIH_tmp=(((HIH_buff[2]<<8)+HIH_buff[3])>>2);

                HIH_tmp/=(1<<14)-2;
                HIH_tmp=HIH_tmp*165-40;
            }
            temp[i] = HIH_tmp*10 + 500;
            hum[i]  = HIH_rh*10;
            n_meas++;
        }
        else {
            temp[i] = 0;
            hum[i]  = 100;
        }
    }
    
    if (mux_2_2){
        for (int i = 9; i< 12 ;i++){
            if ((hih_status >> i)&1){
                
                // turn off mux 2:
                if (mux_2){
                    MUX_buff[0] = 0;
                    i2c_writer(2,mux_2,MUX_buff,1);    
                }
                MUX_buff[0] = 4+(i%4);
                int mux_id = 2;
                int mux_add = mux_2_2;
                
                int status = i2c_writer(mux_id,mux_add,MUX_buff,1);    // Select that line
                
                status    &= i2c_writer(mux_id,HIH_ADDR,MUX_buff,0);  // Say hi to HIH
                CyDelay(100);
                status    &= i2c_reader(mux_id,HIH_ADDR,HIH_buff,4);
                float HIH_rh = 100;
                float HIH_tmp = -50;
                
                if (status){
                    HIH_rh=100*(((HIH_buff[0]&0x3f)<<8)+HIH_buff[1]);
                    HIH_rh/=(1<<14)-2;
                    HIH_tmp=(((HIH_buff[2]<<8)+HIH_buff[3])>>2);

                    HIH_tmp/=(1<<14)-2;
                    HIH_tmp=HIH_tmp*165-40;
                }
            
    

            
                temp[8] = HIH_tmp*10 + 500;
                hum[8]  = HIH_rh*10;
                break;
            }
        }
        //Turn off
        MUX_buff[0] = 0;
        i2c_writer(2,mux_2_2,MUX_buff,1);
    }
    else{
        temp[8] = 0;
        hum[8]  = 100;
    }
    return n_meas;
    
}



//void setup_mux();

void read_HIH(int i2c_id, int sensor_id, int * temp, int * hum){
    
    
}