/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/

/* [] END OF FILE */

#pragma once
#include "project.h"

int test_read(uint32_t * temp, uint32_t * hum);
void setup_HIH_I2C();
int i2c_reader(int channel, uint8_t add, uint8_t * buff, int n_bytes);
int i2c_writer(int channel, uint8_t add, uint8_t * buff, int n_bytes);