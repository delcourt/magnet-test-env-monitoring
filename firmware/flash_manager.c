
#include "project.h"
#include "flash_manager.h"
#include "TCA9534.h"
#include "utils.h"

// For event storage :
#include "ds3232.h"           // Get flash read/write positions
#include "event_manager.h"    // Get event size


#define BUFF_SIZE BLOCK_SIZE+5 // BLOCK_SIZE + 4 add + 1 cmd

#define FL512_FLASH_SIZE 0x4000000 // 64Mb 


uint8_t buf_in[BUFF_SIZE];
uint8_t buf_out[BUFF_SIZE];


uint32_t r_add = 0xFFFFFFFF;
uint32_t w_add = 0xFFFFFFFF;


int assert_equals(uint8_t val, uint8_t expected){
    //Assert that two values are the same.
    //Warning : can lock µC in inf loop.
    
    if (val != expected){
        print_first_line("SPIM ERR : ");
        snprintf(LCD_buf, 17,"Got %2X != %2X",val,expected);
        print_second_line(LCD_buf);
        inf_loop();
        return(0);
    }
    return (1);
}

int flush(){
    //Flushes data in readout buffer. Returns 0 if data found.
    int return_code = 1; 
    while(SPIM_flash_ReadRxStatus() != 0){
            SPIM_flash_ReadRxData();
            return_code = 0;
    }
    return return_code;
}

void process_data(uint8_t * data_in, uint8_t * data_out, int n_bits){
    flush();
    
    int bits_read = 0;
    for (int bits_sent = 0; bits_sent < n_bits; bits_sent++){
        while (SPIM_flash_ReadRxStatus() != 0){
            data_out[bits_read] = SPIM_flash_ReadRxData();
            bits_read++;
        }
        while ((SPIM_flash_ReadTxStatus()&0x4) == 0);
        SPIM_flash_WriteTxData(data_in[bits_sent]);
    }
    
    while (bits_read < n_bits){
        while (SPIM_flash_ReadRxStatus() == 0);
        data_out[bits_read] = SPIM_flash_ReadRxData();
        bits_read++;
    }
}

int wait_WIP(int max_delay){
    int loop_breaker = 0;
    buf_out[1] = 1;
    while((buf_out[1] & 1) != 0){
        buf_in[0] = 0x05;             // Read status register
        buf_in[1] = 0x00;
        process_data(buf_in,buf_out,2);    
        CyDelay(10);
        loop_breaker+=10;
        if (loop_breaker > max_delay){
            return 0;
        }
    }
    return 1;
}

int wait_WE(int max_delay){
    int loop_breaker = 0;
    buf_out[1] = 0;
    while((buf_out[1] & 0x2) != 0x2){
        buf_in[0] = 0x05;             // Read status register
        buf_in[1] = 0x00;
        process_data(buf_in,buf_out,2);    
        loop_breaker+=10;
        CyDelay(10);
        if (loop_breaker > max_delay){
            return 0;
        }
    }
    return 1;
}

int enable_write(int max_delay){
    buf_in[0] = 0x06;
    process_data(buf_in,buf_out,1); //Write enable
    
    if (!wait_WE(max_delay)){
        return 0;
    }
    return 1;
}

int read_flash(uint32_t address, uint8_t * data, int n_bytes){  
    
    if (!wait_WIP(1000)){
        print_first_line("FLASH READ");
        print_second_line("ERROR");
        CyDelay(500);
        return 0;
    }
    
    memset(buf_in,0x00,BUFF_SIZE);         //resetting memory
    memset(buf_out,0x00,BUFF_SIZE);
    buf_in[0] = 0x13;
    buf_in[1] =  address >> 24;
    buf_in[2] = (address >> 16) & 0xFF;
    buf_in[3] = (address >> 8 ) & 0xFF;
    buf_in[4] = (address      ) & 0xFF ;
    
    process_data(buf_in,buf_out,n_bytes+5);

    
    for (int i = 0; i < n_bytes; i++){
        data[i] = buf_out[i+5];
    }
    return 1;
}

int write_flash(uint32_t address, uint8_t * data, int n_bytes){
    //Write n_bytes of data to page "address". Returns 0 if error found.
    // WARNING !!! NO PAGE CHECK PERFORMED
    
    memset(buf_in,0x00,BUFF_SIZE);         //resetting memory
    memset(buf_out,0x00,BUFF_SIZE);
    
    
    if (wait_WIP(1000) == 0 || enable_write(1000) == 0){
        print_first_line("FLASH WRITE");
        print_second_line("ERROR");
        CyDelay(500);
        return 0;
    }
    
    
    buf_in[0] = 0x12;  //Write cmd
    buf_in[1] =  address >> 24;
    buf_in[2] = (address >> 16) & 0xFF;
    buf_in[3] = (address >> 8 ) & 0xFF;
    buf_in[4] = (address      ) & 0xFF ;
    
    for (int i = 0; i < n_bytes; i++){
        buf_in[i+5] = data[i];
    }
    
    process_data(buf_in,buf_out,n_bytes+5);
    return 1;
}

int erase_sector(uint32_t sector){   
    
    memset(buf_in,0x00,BUFF_SIZE);         //resetting memory
    memset(buf_out,0x00,BUFF_SIZE);
    
    if (wait_WIP(1000) == 0 || enable_write(1000) == 0){
        print_first_line("ERASE SECTOR");
        print_second_line("ERROR");
        CyDelay(500);
        return 0;
    }
    
    buf_in[0] = 0xDC; // Sector erase
    buf_in[1] =  sector >> 24;
    buf_in[2] = (sector >> 16) & 0xFF;
    buf_in[3] = (sector >> 8 ) & 0xFF;
    buf_in[4] = (sector      ) & 0xFF;
    
    process_data(buf_in,buf_out,5);
    
    return 1;
}


void flash_init(){
    SPIM_flash_Start();
    HOLD_flash_Write(1);
    DIR023_flash_Write(1);
    DIR1_flash_Write(0);

    
    w_add = RTC_GetFlashWrite();
    r_add = RTC_GetFlashRead();
}


int get_next_block_read(uint32_t old_read_add){
    int head_room = (((old_read_add >> 18)+1) <<18 ) - old_read_add;
    int n_evt_in_block = head_room/EVT_SIZE;
    int last_pointer_in_block = old_read_add + n_evt_in_block*EVT_SIZE;
    int new_add = last_pointer_in_block;
    
    if ((new_add >> 18) == (old_read_add >>18)){
        new_add += EVT_SIZE;
    }
    
    if (new_add >= FL512_FLASH_SIZE)
        new_add = 0;
    
    if ((new_add >>18) == (old_read_add >> 18)){
        print_first_line("E : GETTING BLOCK");
        return 0;
    }
    
    return new_add;
}



int flash_store_event(uint8_t * event){
    
    //Get write and read add from RAM
    if (r_add == 0xFFFFFFFF || w_add == 0xFFFFFFFF)
        flash_init();
    
    // Write at write add
    // Check if event has to be split
    
    int write_success = 1;    
    if ((w_add + EVT_SIZE - 1)>>9 == w_add>>9){
        write_success &= write_flash(w_add,event,EVT_SIZE);
    }
    else{    
        int size_first_page = 0x200 - (w_add&0x1FF); // Trust me
        write_success &= (write_flash(w_add,event,size_first_page));
        write_success &= (write_flash(w_add+size_first_page,event+size_first_page,EVT_SIZE-size_first_page));
    }
    
    if(!write_success){
            print_first_line("CANNOT STORE EVENT");
            CyDelay(500);
            return 0;   
    }
    
    // Prepare next write : 
    w_add += EVT_SIZE; // Start of next write
    
    
    // Check if reached end of flash : 
    if (w_add + EVT_SIZE >= FL512_FLASH_SIZE){
        // Erase first block, move read pointer and set write add back to 0
        erase_sector(0);
        r_add = get_next_block_read(0);    // Force r_add to be second block
        w_add = 0;
    }
    
    if (w_add+EVT_SIZE > r_add){
        if (r_add != 0){           
            // Only remove block if we are not during first writing loop
            erase_sector(r_add&0xFFFC0000);
            r_add = get_next_block_read(r_add); // Moving to next block.
        }
    }
    
    return RTC_SetFlashWrite(w_add) & RTC_SetFlashRead(r_add);
}

int flash_get_n_events(){
    
    if (r_add == 0xFFFFFFFF || w_add == 0xFFFFFFFF)
        flash_init();
        
    // number of events = 
    // N full blocks * n_evt_per_block + Nbits in last block / evt_size
    int N_full =  (w_add>>18);
    if (w_add < r_add){
        N_full = 255;
    }
    
    return (N_full * (256*BLOCK_SIZE) + (w_add &0x3FFFF))/EVT_SIZE;
}


uint16_t flash_get_n_block(){
    if (r_add == 0xFFFFFFFF || w_add == 0xFFFFFFFF)
        flash_init();
        
    if (w_add < r_add){
        return 255*256 + (int)(1+(w_add & 0x3FFFF)/BLOCK_SIZE);
    }
    else{
        return (uint16_t) (1+w_add/BLOCK_SIZE);
    }   
}

int flash_get_block(uint32_t block_id, uint8_t * buffer){
    if (r_add == 0xFFFFFFFF || w_add == 0xFFFFFFFF)
        flash_init();

    
    uint32_t address = (block_id*BLOCK_SIZE + r_add)%0x4000000;    
    read_flash(address,buffer,BLOCK_SIZE);
    
    return 1;
}


int reset_flash(){
    int t = 50;
    int accept = 0;
    while(t > 0 && accept == 0){
            if ((t%10) == 0){    
                print_first_line("Press sw to del");
                snprintf(LCD_buf, 17,"flash. %ds left",t/10);
                print_second_line(LCD_buf);
            }
            led_kprog_Write(!led_kprog_Read());
            LED_Set(t%4);
            CyDelay(100);
            t-=1;
            accept = SW_Get();
    }
    
    if (accept == 0){
        return 0;
    }
    print_first_line("DELETING FLASH!");
    bulk_erase();
    print_second_line("RESET POINTERS");
    RTC_SetFlashRead(0);
    RTC_SetFlashWrite(0);
    r_add = 0;
    w_add = 0;
    return 1;
}

int bulk_erase(){
   
    memset(buf_in,0x00,BUFF_SIZE);         //resetting memory
    memset(buf_out,0x00,BUFF_SIZE);
    
    if (wait_WIP(1000) == 0 || enable_write(1000) == 0){
        print_first_line("BULK ERASE");
        print_second_line("ERROR");
        CyDelay(500);
        return 0;
    }
    print_first_line("Ready to erase");
    
    
    buf_in[0] = 0x60;  //Write cmd
    process_data(buf_in,buf_out,1);
    
    print_second_line("Erasing...");
    print_second_line("Busy...");
    wait_WIP(60000);
    print_second_line("DONE ! :-)");
    return 0;
}


void flash_recreate_index(){
    if (r_add == 0){
        // Look for first event that starts with 0xFF
        int n_events_max = FL512_FLASH_SIZE/EVT_SIZE;
        int n_bits = 1;
        while ((1<<n_bits) < n_events_max)
            n_bits++;
        uint8_t bb;
        int event_number = 0;
        int bit = n_bits;
        while (bit > -1){
            event_number += (1<<bit);  
            if (read_flash(event_number*EVT_SIZE,&bb,1) == 0xFF)
                event_number -= (1<<bit);
            bit --;
        }
        w_add = (event_number+1)*EVT_SIZE;
        
    }
    else{
        // Flash is full 
        // -> w_add should be first event starting with 0xFF from previous block
        // TODO !
        
    }
    
    RTC_SetFlashWrite(w_add);
    
}
