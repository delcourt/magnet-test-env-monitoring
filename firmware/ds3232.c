/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/
#include "project.h"
#include "utils.h"
#include "ds3232.h"
#include "configuration.h"
#include <time.h>
#include <stdio.h>


/*
int rtc_read(int add, uint8_t * buff, int n_bytes){
    I2C_rtc_MasterClearStatus();
    I2C_rtc_MasterReadBuf(add, buff, n_bytes, I2C_rtc_MODE_COMPLETE_XFER);
    int breaker = 0;
    while(!(I2C_rtc_MasterStatus()&I2C_rtc_MSTAT_RD_CMPLT)){
        breaker++;
        if (breaker > 1000)
            return 0;
    }
    return !(I2C_rtc_MasterStatus() & I2C_rtc_MSTAT_ERR_ADDR_NAK);
}

int rtc_write(int add, uint8_t * buff, int n_bytes){
    I2C_rtc_MasterClearStatus();
    I2C_rtc_MasterWriteBuf(add, buff, n_bytes, I2C_rtc_MODE_COMPLETE_XFER);
    int breaker = 0;
    while(!(I2C_rtc_MasterStatus()&I2C_rtc_MSTAT_WR_CMPLT)){
        breaker++;
        if (breaker > 1000){
            return 0;
        }
    }
    return !(I2C_rtc_MasterStatus() & I2C_rtc_MSTAT_ERR_ADDR_NAK);
} */

/*
int rtc_decode(int word){
    return 10*(word>>4) + (word&0xF);
}*/


#define DEFAULT_RTC_REG_0E 0x00
#define DEFAULT_RTC_REG_0F 0x08

void RTC_Init() {
    I2C_rtc_MasterClearStatus();
    I2C_rtc_MasterClearWriteBuf();
    
    // Checking state of RTC : 
    uint8_t status = RTC_Read(0x0E);
    if (status != DEFAULT_RTC_REG_0E){
        print_first_line("Bad RTC reg!");
        snprintf(LCD_buf, 17,"0x0E %X->%X",status,DEFAULT_RTC_REG_0E);
        print_second_line(LCD_buf);
        RTC_Write(0x0E,DEFAULT_RTC_REG_0E);
        CyDelay(2000);
        if (RTC_Read(0x0E) != DEFAULT_RTC_REG_0E ){
            print_first_line("RTC ERROR :");
            print_second_line("setting 0x0E");
            inf_loop();
        }
    }
    status = RTC_Read(0x0F)&0xF8; // Ignore busy and alarm bits
    if (status != DEFAULT_RTC_REG_0F){
        if (status>>7 == 1){
            print_first_line("RTC oscillator");
            print_second_line("in error !");
            CyDelay(2000);
        }
        if ((status &0x7F) != DEFAULT_RTC_REG_0F){
            print_first_line("Bad RTC reg!");
            snprintf(LCD_buf, 17,"0x0F %X->%X",status,DEFAULT_RTC_REG_0F);
            print_second_line(LCD_buf);
            CyDelay(2000);
        }
        RTC_Write(0x0F,DEFAULT_RTC_REG_0F);
        CyDelay(200);
        status = RTC_Read(0x0F)&0xF8;
        if (status != DEFAULT_RTC_REG_0F ){
            print_first_line("RTC ERROR :");
            snprintf(LCD_buf, 17,"0x0F %X!=%X",status,DEFAULT_RTC_REG_0F);
            print_second_line(LCD_buf);
            inf_loop();
        }
    }
    
    #ifdef DS32_EEPROM_BAK
        EEPROM_Start();    
    #endif
    
}

void RTC_DisplayTime(){
    struct tm t = RTC_to_tm();
    snprintf(LCD_buf, 17,"%d/%d/%d %d:%d:%d",t.tm_year%100,t.tm_mon +1 ,t.tm_mday,t.tm_hour,t.tm_min,t.tm_sec);
    print_second_line(LCD_buf);    
}



uint8 RTC_Read(uint8 rtcaddr) {//read on ly 1 register
    volatile uint32 error;
    uint8 value;
    uint8 addr=rtcaddr;
   // uint8 wbuff[8],rbuff[8]; 
    I2C_rtc_MasterClearStatus();
    error=I2C_rtc_MasterWriteBuf(DS3232_ADDR,&addr,1,I2C_rtc_MODE_NO_STOP);//I2C_rtc_MODE_REPEAT_START
    while(!((error=I2C_rtc_MasterStatus())&I2C_rtc_MSTAT_WR_CMPLT));
    error=I2C_rtc_MasterReadBuf(DS3232_ADDR,&value,0x1,I2C_rtc_MODE_REPEAT_START);//
    while(!((error=I2C_rtc_MasterStatus())&I2C_rtc_MSTAT_RD_CMPLT));
    
 return value;   
}

uint8 RTC_Write(uint8 rtcaddr, uint8 rtcvalue) {//write 1 register
    volatile uint32 error;
    //uint8 last_mot;
    //uint8 addr=last_mot_ADDR;
    uint8 wbuff[8];
    I2C_rtc_MasterClearStatus();
    wbuff[0]=rtcaddr;
    wbuff[1]=rtcvalue;
    //wbuff[2]=0;
    error=I2C_rtc_MasterWriteBuf(DS3232_ADDR,wbuff,2,I2C_rtc_MODE_COMPLETE_XFER);//I2C_rtc_MODE_REPEAT_START
  //  while(error=I2C_rtc_MasterStatus()!=I2C_rtc_MSTAT_WR_CMPLT);
    while(!((error=I2C_rtc_MasterStatus())&I2C_rtc_MSTAT_WR_CMPLT));
  //  error=I2C_rtc_MasterReadBuf(DS3232_ADDR,&last_mot,0x1,I2C_rtc_MODE_REPEAT_START);//
   // while(error=I2C_rtc_MasterStatus()!=I2C_rtc_MSTAT_RD_CMPLT);
   // while(!((error=I2C_rtc_MasterStatus())&I2C_rtc_MSTAT_RD_CMPLT));
    
 return 0;   
}

struct tm rtc_time;
struct tm rtc_set;
char rtc_buffer[0x14];


char bcd2int(char bcd) {
    return (bcd&0xf)+(10*(bcd>>4));
}

char int2bcd(uint8 int_val) {//weird things happens above 99
 return (int_val%10)|((int_val/10)<<4);   
    
}

void RTC_Reset(){
    RTC_Write(0,0);
    RTC_Write(1,0);
    RTC_Write(2,0);
}


struct tm RTC_to_tm() {
    uint8 error;
    uint8 zero=0;
    uint8_t rtc_buffer[18];
    memset(rtc_buffer,0,sizeof(rtc_buffer));

    I2C_rtc_MasterClearStatus();
    error=I2C_rtc_MasterWriteBuf(DS3232_ADDR,&zero,1,I2C_rtc_MODE_NO_STOP);//I2C_rtc_MODE_REPEAT_START
    while(!((error=I2C_rtc_MasterStatus())&I2C_rtc_MSTAT_WR_CMPLT));
    error=I2C_rtc_MasterReadBuf(DS3232_ADDR,rtc_buffer,7,I2C_rtc_MODE_REPEAT_START);//
    while(!((error=I2C_rtc_MasterStatus())&I2C_rtc_MSTAT_RD_CMPLT));
    
    
    
    struct tm rtc_time;

    //read from DS3231
    rtc_time.tm_sec=bcd2int(rtc_buffer[0]);
    rtc_time.tm_min=bcd2int(rtc_buffer[1]);
    rtc_time.tm_hour=bcd2int(rtc_buffer[2]&0x3f);//handle 12/24
    
   
    rtc_time.tm_mday=bcd2int(rtc_buffer[4]);//DS is 1 to 31, tm is 1 to 31
    rtc_time.tm_mon=bcd2int(rtc_buffer[5]&0x1f)-1;//DS is 1 to 12, tm is 0 to 11
    rtc_time.tm_wday=bcd2int(rtc_buffer[3])-1;//DS is 1 to 7, tm is 0 to 6
    rtc_time.tm_year=bcd2int(rtc_buffer[6])+100*(rtc_buffer[5]>>7);
        
    return rtc_time;
}

uint8 tm_to_RTC(struct tm * t) {
    uint8_t rtc_buf[8];
    rtc_buf[0] = 0;                     //Address
    rtc_buf[1] = int2bcd(t->tm_sec);    //seconds
    rtc_buf[2] = int2bcd(t->tm_min);    //minutes
    rtc_buf[3] = int2bcd(t->tm_hour);   //hour in 24h format
    rtc_buf[4] = 1;                     //Ignoring week day
    rtc_buf[5] = int2bcd(t->tm_mday);   //day of the month (1-31)
    rtc_buf[6] = ((t->tm_year > 100)<<7) + int2bcd(t->tm_mon+1);  //century bit + month(+1 offset)
    rtc_buf[7] = int2bcd((t->tm_year)%100); //year
    
    uint8 error=I2C_rtc_MasterWriteBuf(DS3232_ADDR,rtc_buf,8,I2C_rtc_MODE_COMPLETE_XFER);//I2C_rtc_MODE_REPEAT_START
    while(!((error=I2C_rtc_MasterStatus())&I2C_rtc_MSTAT_WR_CMPLT));
    return error;
}



uint8 RTC_Read_UM(uint8 addr, uint8 len, uint8_t *buff){//read user memory
    
    uint8 reg_add = addr+0x14;
    
    I2C_rtc_MasterClearStatus();
    I2C_rtc_MasterClearReadBuf();
    
    uint8_t error=I2C_rtc_MasterWriteBuf(DS3232_ADDR,&reg_add,1,I2C_rtc_MODE_NO_STOP);
    while(!((error=I2C_rtc_MasterStatus())&I2C_rtc_MSTAT_WR_CMPLT));
    
    error=I2C_rtc_MasterReadBuf(DS3232_ADDR,buff,len,I2C_rtc_MODE_REPEAT_START);
    while(!((error=I2C_rtc_MasterStatus())&I2C_rtc_MSTAT_RD_CMPLT));
    
    return !(error & I2C_rtc_MSTAT_ERR_XFER);
}

uint8 RTC_Write_UM(uint8 addr, uint8 len, uint8_t *buff) {//write user memory
    uint8 wbuff[len+1];
    I2C_rtc_MasterClearWriteBuf();
    wbuff[0]=addr+0x14;
    memcpy(wbuff+1,buff,len);
    
    I2C_rtc_MasterClearStatus();
    
    uint8_t error=I2C_rtc_MasterWriteBuf(DS3232_ADDR,wbuff,len+1,I2C_rtc_MODE_COMPLETE_XFER);
    while(!((error=I2C_rtc_MasterStatus())&I2C_rtc_MSTAT_WR_CMPLT));
    
    return !(error & I2C_rtc_MSTAT_ERR_XFER);
}

int32_t RTC_ReadSRAM(int addr){
    if (addr > 57){
        return 0;
    }
    uint8_t buff[4];
    RTC_Read_UM(4*addr,4,buff);
    int32_t value = (int) ((buff[0]<<24) + (buff[1]<<16) + (buff[2]<<8) + buff[3]);
    return value;
}

uint8_t  RTC_WriteSRAM(int addr, int32_t value){
    if (addr > 57){
        return 1;
    }
    

    
    uint8_t buff[4];
    uint32_t uval = (uint32_t) value;
    buff[0] = (uval >>24)&0xFF;
    buff[1] = (uval >>16)&0xFF;
    buff[2] = (uval >> 8)&0xFF;
    buff[3] = (uval     )&0xFF;
    
    #ifdef DS32_EEPROM_BAK
        if (addr != 1){ //Ignoring write pointer (changing too often)
            EEPROM_WriteByte(buff[0], 4*addr);
            EEPROM_WriteByte(buff[1], 4*addr + 1);
            EEPROM_WriteByte(buff[2], 4*addr + 2);
            EEPROM_WriteByte(buff[3], 4*addr + 3);
        }
    #endif
    
    return RTC_Write_UM(4*addr, 4, buff);
}


void RTC_SetTimestamp(long int timestamp){
    /* Conversion to time_t as localtime() expects a time_t* */
    time_t epoch_time_as_time_t = timestamp;
    /* Call to localtime() now operates on time_t */
    struct tm * timeinfo = gmtime(&epoch_time_as_time_t);
    tm_to_RTC(timeinfo);
}

long int RTC_GetTimestamp(){
    struct tm t = RTC_to_tm();
    return mktime(&t);
}

int32_t read_pointer_cache = 0xFFFFFFFF;
int32_t write_pointer_cache = 0xFFFFFFFF;

int32_t RTC_GetFlashRead(){ 
    if (read_pointer_cache == 0xFFFFFFFF){
        read_pointer_cache = RTC_ReadSRAM(0);
    }
    return read_pointer_cache;
}

int32_t RTC_GetFlashWrite() {
    if (read_pointer_cache == 0xFFFFFFFF){
        write_pointer_cache = RTC_ReadSRAM(1);
    }
    return write_pointer_cache;

}

int8_t  RTC_SetFlashRead(uint32_t value){
    if (value == read_pointer_cache)
        return 1;
    read_pointer_cache = value;    
    return RTC_WriteSRAM(0, value);
}
int8_t  RTC_SetFlashWrite(uint32_t value){
    if (value == write_pointer_cache)
        return 1;
    write_pointer_cache = value;
    return RTC_WriteSRAM(1, value);
}




int32_t time_offset = 0; // timestamp when 1Hz counter was reset

uint32_t TM_GetUnixTime(){
    int counter = Counter_1Hz_ReadCounter();
    if (counter > 0)
        return time_offset + counter + 1;
    else
        return RTC_GetTimestamp();
}

void TM_SetUnixTime(uint32_t xx){
    RTC_SetTimestamp(xx);
    time_offset = xx;
    Counter_1Hz_WriteCounter(0);
    Counter_1Hz_Start();
}

void TM_Start(){
    RTC_Init();
    Counter_1Hz_Start();
    time_offset = RTC_GetTimestamp();
}

char TM_TimeChar_ [17];

void TM_DisplayTime(int line){
    time_t epoch_time_as_time_t = TM_GetUnixTime() + get_conf()->timezone;
    struct tm * t = gmtime(&epoch_time_as_time_t);
    snprintf(TM_TimeChar_, 17,"%d/%d/%d %d:%d:%d",t->tm_year%100,t->tm_mon +1 ,t->tm_mday,t->tm_hour,t->tm_min,t->tm_sec);
    if (line == 2)
        print_second_line(TM_TimeChar_);
    else
        print_first_line(TM_TimeChar_);
}


void TM_SetTimezone(int n_sec){
    get_conf()->timezone = n_sec;
    write_conf();
}

int  TM_GetTimezone(){
    return get_conf()->timezone;
}
