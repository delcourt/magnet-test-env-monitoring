/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/

#include "project.h"
#include "pt1000.h"
#include "math.h"
//extern PT_array;
const uint8_t ADG_LUT[]={0x08, 0x04, 0x02, 0x01, 0x10, 0x20, 0x80, 0x40 };

void PT_Init() {
     
    for (int i = 0; i< 8; i++){
        PT_array[i] = 0;
    }
    
    //volatile char adg_sel=0;
    //volatile int16 adc_r_mes, adc_pt_mes;
    volatile int16 adc_r_mes;
    uint8_t ADG_buff[2];
     //this is the look up table for connecting the current to the right PT (numbered as on PCB)
    
//    const char ADG_LUT[]={0x01, 0x02, 0x04, 0x08, 0x80, 0x40,0x10, 0x20 };
    
    //start current source
    IDAC8_1_Start();
    IDAC8_1_SetValue(0x7d*0.75);//set to 0.75mA
    
    
    
    //start ADCs
    ADC_R_Start();
    ADC_PT_Start();
    ADC_R_StartConvert();
    ADC_PT_StartConvert();
    
    AMuxSeq_Stop();//disconnect all inside µC
    
    //set the ADG configuration
    I2C_rtc_MasterClearStatus();
    ADG_buff[0]=0;//disconnect all in external mux
    I2C_rtc_MasterWriteBuf(ADG728_ADDR,ADG_buff , 1, I2C_rtc_MODE_COMPLETE_XFER);
    while(!(I2C_rtc_MasterStatus()&I2C_rtc_MSTAT_WR_CMPLT));
    CyDelay(1);//wait for stabilisation
    
    
    //get offset for ADC on R
    adc_r_mes=ADC_R_GetResult16();//measure with no current flowing in R
    ADC_R_SetOffset(adc_r_mes);//set offset
    ADC_R_SetScaledGain(33998);//for some reasons, I had to scale this one to get the right current
    
    //that's it for init
}

void PT_Update_Next() {//will perform one PT measurement and set the Temp in the global PT_array, NaN will be used for results <0 and >250deg Celcius
    
    /*volatile*/ int32 acc_i, acc_v;
    
    /*volatile*/ float i_mes, v_mes;
    /*volatile*/ float r_mes;
    /*volatile*/ float RTD_temp;
    /*volatile*/ int16 adc_r_mes, adc_pt_mes;
    int16 i;
    uint8_t ADG_buff[2];
    static uint8 adg_sel=0;
    //set the right configuration for ADG mux
    I2C_rtc_MasterClearStatus();
    ADG_buff[0]=ADG_LUT[adg_sel];
    I2C_rtc_MasterWriteBuf(ADG728_ADDR,ADG_buff , 1, I2C_rtc_MODE_COMPLETE_XFER);
    while(!(I2C_rtc_MasterStatus()&I2C_rtc_MSTAT_WR_CMPLT));
    
    //set next position on mux_seq
    AMuxSeq_Next();
    CyDelay(1);//should be long enough to charge capacitors, should be closer to 100µs
    
    //initialise averaging accumulator
    acc_i=0;
    acc_v=0;
    for(i=0;i<PT_AVG;i++){
        ADC_R_IsEndConversion(ADC_R_WAIT_FOR_RESULT);
    
        adc_r_mes=ADC_R_GetResult16();
        acc_i+=adc_r_mes;
        ADC_PT_IsEndConversion(ADC_PT_WAIT_FOR_RESULT);
        adc_pt_mes=ADC_PT_GetResult16();
        acc_v+=adc_pt_mes;
    }
    
    //average
    adc_r_mes=acc_i/PT_AVG;
    adc_pt_mes=acc_v/PT_AVG;
    
    
    //get current and volatges
    i_mes=ADC_R_CountsTo_Volts(adc_r_mes)/100;//100 is the external resistor
    v_mes=ADC_PT_CountsTo_Volts(adc_pt_mes);
    
    //compute R from I, V
    r_mes=v_mes/i_mes;
    r_mes -= 1.7; //Correcting for cable length
    
    //get temperature from table
    RTD_temp=(float)RTD_GetTemperature((int32)(r_mes*1000))/100;
    
    if (RTD_temp<0 || RTD_temp>250) RTD_temp=nan("");
    
    //save to global variable
    PT_array[adg_sel]=RTD_temp;
    
    if (adg_sel==0)//for debug
        asm("nop");
    
    adg_sel+=1;
    adg_sel&=0x7;
        
   // return adg_sel;
}

void PT_Update_All() {//will cycle through all PTs and update temp in global array
    for(char i=0;i<8;i++) {
        PT_Update_Next();   
        
    }
    
}

/* [] END OF FILE */
