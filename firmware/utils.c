#include "utils.h"
#include "LCD.h"
#include "ds3232.h"
#include "project.h"

uint32_t timer_t0;
int never_read = 1;


void Start_timer(){
    never_read = 1;
    Timer_Start();
    Timer_WriteCounter(-1);
    Timer_SoftwareCapture();
}

uint32_t Get_timer(){
    Timer_SoftwareCapture();
    if (never_read){
        never_read = 0;
        timer_t0 = Timer_ReadCapture();
    }
    return timer_t0 - Timer_ReadCapture();
}


uint32_t Stop_timer(){
    Timer_SoftwareCapture();
    Timer_Stop();
    if (never_read){
        timer_t0 = Timer_ReadCapture();
    }
    return timer_t0 - Timer_ReadCapture();
}



void print_first_line(char * txt){
    LCD_Position(0,0);
    I2C_LCD_PrintString("                ");
    LCD_Position(0,0);
    I2C_LCD_PrintString(txt);
}

void print_second_line(char * txt){
    LCD_Position(0,1);
    I2C_LCD_PrintString("                ");
    LCD_Position(0,1);
    I2C_LCD_PrintString(txt);
}

void inf_loop(){
    if (sw_kprog_Read() == 0){
        CyDelay(500);
    }
    
    while(sw_kprog_Read() == 1){
        led_kprog_Write(~led_kprog_Read());
        CyDelay(100);
    }
    led_kprog_Write(0);
}