/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/

/* [] END OF FILE */
#ifndef TCA9534_H
#define TCA9534_H
    //I2C addr
    #define TCA9534_ADDR   (0x20)
    
    //registers
    #define TCA9534_IPORT   (0)
    #define TCA9534_OPORT   (1)
    #define TCA9534_POLINV   (2)
    #define TCA9534_CONF   (3)
    
    //outputs
    #define O_EN0  (0x04)
    #define O_EN1  (0x01)
    #define O_D4   (0x20)
    #define O_D3   (0x10)
    
    //inputs
    #define I_OC0  (0x08)
    #define I_OC1  (0x02)
    #define I_SW1  (0x40)
    #define I_SW2  (0x80)
    
    #define TCA9534_DEFCONF (0b11001010) //default in/out magic
   
    
void IO_Init(); //init IO expander register with right direction and initial values

char O_Set(char mask); //set output register, but irrelevant bits are masked (should have no side effect anyway)

char REG_Get(char reg); //read a register from the IO exp

char I_Get(); //get input register
char SW_Get(); //get switches value reordered to be at LSB position

void MUX_Set(char value); //enable mux0/1, can pass mask or 2 bit value

void LED_Set(char value); //set leds, can pass mask or 2 bit value
    
#endif
    