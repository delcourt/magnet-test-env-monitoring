#pragma once

#define BLOCK_SIZE 1024 //flash block read-out size. /!\ must be divider of 256*1024


void flash_init();

int flash_store_event(uint8_t * event);

int flash_get_block(uint32_t block_id, uint8_t * buffer);
uint16_t flash_get_n_block(); //Get number of blocks in memory

int flash_get_n_events();
//Get number of events stored in flash

int bulk_erase();

int reset_flash();

void flash_recreate_index();