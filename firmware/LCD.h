/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/

#include "project.h"

#ifndef LCD_H
#define LCD_H
    
    //It should be x2 for the I2C_LCD block on schematics, this is important
     #define LCD_ADDR    (0x3e)

    void LCD_Control(uint8 control);
    
    void LCD_Init();//must be called AFTER initialising the corresponding I2C block
    void LCD_Position(uint8 x, uint8 y);//lcd position on screen, 0,0 
    
#endif

/* [] END OF FILE */
