#pragma once
#include "project.h"
#include <stdio.h>

#define EVT_SIZE   48
#define EVT_BUFF_DEPTH   50

#define VSEL_ON  1
#define VSEL_OFF 0

uint8_t     evt_buff[EVT_BUFF_DEPTH][EVT_SIZE];
uint8_t     evt_buff_n;
uint        evt_buff_offset;
uint        evt_buff_overflow;

void set_dummy_event(int x);
void generate_event(int event_number);

void start_event_manager();
void fill_buff(uint8_t * buff, int val, int nBits, int * pos);
void reset_buffer(uint8_t * buff);

int  get_vsel_bits(uint32_t * humidities);