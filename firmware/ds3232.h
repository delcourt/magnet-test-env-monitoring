/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/

#ifndef DS3232_H
#define DS3232_H
#include <stdio.h>
//works also for DS3231 but wihtout user memory
    
#define DS3232_ADDR_R (0b11010001)
#define DS3232_ADDR_W (0b11010000)
#define DS3232_ADDR   (0b1101000)

//register def, see doc for meaning
#define DS32_SEC    (0)
#define DS32_MIN    (1)
#define DS32_HOUR      (2)
#define DS32_DOW      (3)
#define DS32_DOM      (4)
#define DS32_MONTH      (5)
#define DS32_YEAR      (6)
#define DS32_AM1SEC      (7)
#define DS32_AM1MIN      (8)
#define DS32_AM1HOUR      (9)
#define DS32_AM1DAY      (10)
#define DS32_AM2MIN      (11)
#define DS32_AM2HOUR      (12)
#define DS32_AM2DAY      (13)
#define DS32_CONTROL     (14)
#define DS32_STATUS      (15)
#define DS32_AGING      (16)
#define DS32_TEMPH      (17)
#define DS32_TEMPL      (18)
#define DS32_TEST      (19)
       
    
    
#define DS32_EEPROM_BAK // If defined, will duplicate RAM content to EEPROM.
                        // Write RAM pointer is ignored.
                        
int32_t  RTC_ReadSRAM(int addr);
uint8_t  RTC_WriteSRAM(int addr, int32_t value);

// Flash read and write pointers
int32_t RTC_GetFlashRead();
int32_t RTC_GetFlashWrite();

int8_t  RTC_SetFlashRead(uint32_t value);
int8_t  RTC_SetFlashWrite(uint32_t value);

// Wrapper for time managment :

uint32_t TM_GetUnixTime();
void TM_SetUnixTime(uint32_t xx);
void TM_Start();
void TM_Refresh();
void TM_DisplayTime(int line);
void TM_SetTimezone(int n_sec);
int  TM_GetTimezone();

// Low level functions, only use if you know what you are doing :

void RTC_Init();                                      //Initialise RTC
uint8_t RTC_Read(uint8_t rtcaddr);                    //read from  RTC at given addr
uint8_t RTC_Write(uint8_t rtcaddr, uint8_t rtcvalue); //write to   RTC at givenaddr

void      RTC_Reset();
void      RTC_SetTimestamp(long int);
long int  RTC_GetTimestamp();
void      RTC_DisplayTime();
struct tm RTC_to_tm();

#endif
