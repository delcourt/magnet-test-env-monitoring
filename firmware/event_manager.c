
#include "event_manager.h"
#include "flash_manager.h"
#include "configuration.h"
#include "utils.h"
#include "pt1000.h"
#include "therm_reader.h"
#include "ds3232.h" // Time management

int vsel_signal;

int eeprom_calib_temp = 0;

void reset_buffer(uint8_t * buff){
    for(int i = 0; i<EVT_SIZE; i++){
        buff[i] = 0;
    }
}



void fill_buff(uint8_t * buff, int val, int nBits, int * pos){   
    // bit by bit version : 
    /*
    for (int i = 0; i<nBits; i++){
        if (*pos > 8*48-1)
            return;
        buff[*pos/8] |= ((val>>(nBits-i-1))%2)<<(7-*pos%8);
        *pos = *pos +1;
    }*/
   
   
    // byte by byte version :
    while(nBits > 0){
       int byte_index = *pos/8;
       if (byte_index > 48)
            return;
        
       int offset = (*pos)%8;
       int n_bits_to_add = nBits;
       if (n_bits_to_add > 8-offset)
           n_bits_to_add = 8-offset;
       
       buff[byte_index] |= ((val >> (nBits-n_bits_to_add)) % (1<<n_bits_to_add)) << (8-offset-n_bits_to_add);
       
       *pos = *pos +n_bits_to_add;
       nBits-= n_bits_to_add;
    }
}
    
void generate_event(int event_number){
    
    uint8_t * event = evt_buff[evt_buff_offset%EVT_BUFF_DEPTH];
    memset(event,0,EVT_SIZE);
    evt_buff_offset ++ ;
    evt_buff_n ++ ; 
    if (evt_buff_n > EVT_BUFF_DEPTH){
        evt_buff_n = EVT_BUFF_DEPTH;
        evt_buff_overflow = 1;
    }
    
    int pos = 0;
    uint8_t header = (1<<7) | evt_buff_n;
    fill_buff(event,header , 8, &pos); 
    
    uint32_t timestamp = TM_GetUnixTime();
    fill_buff(event,timestamp , 32, &pos);   //timestamp
    
    uint32_t temp_buff[9];
    uint32_t hum_buff[9];
    
    for (int i = 0; i < 9; i++){
        temp_buff[i] = 0;
        hum_buff[i]  = 0;
    }
    
    test_read(temp_buff,hum_buff);
    
    for (int i = 0; i<9; i++){
        fill_buff(event,temp_buff[i],11,&pos);
        fill_buff(event,hum_buff[i],10,&pos);
    }
    
    /*
    THIS CANNOT BE DONE IN THIS FIRMWARE VERSION
    TTL connectors are used as input only
    
    int vsel_bits = get_vsel_bits(hum_buff);
    int vsel_changed = 0;
    if (vsel_signal == VSEL_ON && (vsel_bits & 2)){
        vsel_signal = VSEL_OFF;
        vsel_changed = 1;
    }
    else if (vsel_signal == VSEL_OFF && (vsel_bits & 1)){
        vsel_signal = VSEL_ON;
        vsel_changed = 1;
    }
    
    if (vsel_changed){
        int vsel_lemo_id = get_conf()->lemo_routing & 0x3;
        snprintf(LCD_buf, 17,"Read: %X",LEMO_Read());
        print_first_line(LCD_buf);
        uint32_t ww = (LEMO_Read() & (0xFF ^ (1<<vsel_lemo_id))) | (vsel_signal << vsel_lemo_id);
        //LEMO_Write(0xFF*vsel_signal);
        LEMO_Write(ww);
        //snprintf(LCD_buf, 17,"Write: %X",ww);
        //print_second_line(LCD_buf);
    }
    */
    
        
    PT_Update_All();
    
    for (int sensor_id = 0; sensor_id < 8; sensor_id++){
        float t = PT_array[sensor_id];
        int val = t*10 + 500;
        fill_buff(event,val,11, &pos);
    }
    
    int16_t temp;
    DieTemp_GetTemp(&temp);
    
    #ifdef DS32_EEPROM_BAK
        if (abs(temp-eeprom_calib_temp) > 5){
            eeprom_calib_temp = temp;
            EEPROM_UpdateTemperature();
        }
    #endif
    
    uint32_t tt = (temp+50)*10;
    fill_buff(event,tt,11, &pos);
    
    // Adding LEMO counters
    for (int i = 0; i < 4; i++){
        int count = lemo_get_counts(i);
        fill_buff(event,count,16, &pos);
    }
    lemo_reset();
    
    
    // Check if should store in flash:
    int prescale = get_conf()->flash_prescale;
    if (prescale > 0 && (event_number%prescale) == 0){
        flash_store_event(event);
    }
}


void set_dummy_event(int x){
    uint8_t * buff = evt_buff[evt_buff_offset%EVT_BUFF_DEPTH];
    reset_buffer(buff);
    
    
    evt_buff_offset ++ ;
    evt_buff_n ++ ; 
    if (evt_buff_n > EVT_BUFF_DEPTH){
        evt_buff_n = EVT_BUFF_DEPTH;
        evt_buff_overflow = 1;
    }
    
    
    
    int pos = 0;
    uint8_t header = (1<<7) | evt_buff_n;
    fill_buff(buff,header , 8, &pos); 
    
    uint32_t timestamp = TM_GetUnixTime();
    fill_buff(buff,timestamp , 32, &pos);   //timestamp
    
    uint32_t temp_buff[9];
    uint32_t hum_buff[9];
    
    for (int i = 0; i < 9; i++){
        temp_buff[i] = 0;
        hum_buff[i]  = 0;
    }
    
    test_read(temp_buff,hum_buff);
    
    for (int i = 0; i<9; i++){
        fill_buff(buff,temp_buff[i],11,&pos);
        fill_buff(buff,hum_buff[i],10,&pos);
    }
    /*
    for (int sensor_id = n_evt; sensor_id < 8; sensor_id++){
        int temp = 0; //100+ sensor_id + 100*sin(timestamp*0.01+sensor_id);
        int hum  = 0; //0.5*temp;   
        fill_buff(buff,temp,11, &pos);
        fill_buff(buff,hum,10, &pos);
    }*/
    
    PT_Update_All();
    
    for (int sensor_id = 0; sensor_id < 8; sensor_id++){
        float t = PT_array[sensor_id];
        int val = t*10 + 500;
        fill_buff(buff,val,11, &pos);
    }
    
    //fake acceleration data
    uint16_t blah = 0xAAAA;
    fill_buff(buff,blah,10, &pos);
    fill_buff(buff,blah,10, &pos);
    
    int16_t temp;
    DieTemp_GetTemp(&temp);
    uint32_t tt = (temp+50)*10;
    fill_buff(buff,tt,11, &pos);
    
    
    
}


void start_event_manager(){
    evt_buff_n = 0;
    evt_buff_offset = 0;
    evt_buff_overflow = 0;   
    vsel_signal = VSEL_OFF;
}

int  get_vsel_bits(uint32_t * humidities){   
    uint32_t thresh_conf = get_conf()->dcs_thresh;
    uint8_t        mask =  thresh_conf & 0xFF;
    uint32_t      t_low = (thresh_conf >> 8) & 0x3FF;
    uint32_t     t_high = (thresh_conf >> 18) & 0x3FF;
    uint8_t         maj = (thresh_conf >> 28) & 0xF;
    
    int n_low  = 0;
    int n_high = 0;
    
    for (int i = 0; i < 8; i++){
        if ((mask >> i) & 1){
            if ((uint32_t) humidities[i] < t_low){
                n_low = n_low + 1;
            }
            if (humidities[i] > t_high)
                n_high ++;
        }
    }
    
    return (n_low >= maj) | ((n_high >= maj) << 1);
    
}
