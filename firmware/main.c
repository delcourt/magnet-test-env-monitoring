/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/
#include "project.h"
#include "TCA9534.h"
#include <time.h>

#include "Acc_reader.h"
#include "event_manager.h"
#include "serial_driver.h"
#include "configuration.h"
#include "utils.h"
#include "pt1000.h"
#include "ds3232.h"
#include "therm_reader.h"
#include "flash_manager.h"
#include "lemo_counter.h"
#include "math.h"
#include "LCD.h"
#include <stdio.h>


/* USB device number. */
#define USBFS_DEVICE  (1u)








int main(void)
{
    CyGlobalIntEnable; /* Enable global interrupts. */
    CyVdLvAnalogEnable(0, 0x0B); //Interupt at 4.45V
    PWM_display_Start();
    PWM_display_WriteCompare(999);
    
    I2C_rtc_Start();
    I2C_rtc_Init();
    
    LCD_Init();
    //I2C_LCD_WriteControl(I2C_LCD_CURSOR_BLINK);

    IO_Init();
    
    print_first_line("IIHE Rh-T\xDF\C");
    print_second_line("FW 211104.2");
    CyDelay(1000);
    
    lemo_init();
    
    TM_Start();             //Time manager
    setup_HIH_I2C();
    PT_Init();
    MUX_Set(3);
    flash_init();
    
    /*TODO
    NIM
    ACC
    */

    
    
    // DEBUG : 
    set_default_conf();

    //reset_flash();
    
    //inf_loop();
    set_default_conf();
    
    print_first_line("Start Serial");
    USBFS_1_Start(1, USBFS_1_5V_OPERATION);   
    if (0u != USBFS_1_IsConfigurationChanged())
    {
        if (0u != USBFS_1_GetConfiguration())
        {
            print_second_line("CDC Init");
            USBFS_1_CDC_Init();
        }
    }
    //while(!USBFS_1_GetConfiguration());
    
    start_event_manager();
    
    //DEBUG : 
    get_conf()->flash_prescale = 2;
    
    
    
    
    print_first_line("Rh-T running !");
    print_second_line("");
    int full_display = 0;
    int event_counter = 0;
    Start_timer();
    char sw_status = 0;
    
    while(1){
        uint32_t dt = Get_timer();
        
        // Halt everything if an external switch is down :
        char sw_pressed = SW_Get();
        if (sw_pressed != sw_status){
            if (sw_pressed){
                print_first_line("RH-T paused !");
                print_second_line("Safe to stop.");
                USBFS_1_CDC_Init();
            }
            else{
                print_first_line("Operations resumed");
                print_second_line("");
                
            }
            sw_status = sw_pressed;
        }
        if (sw_pressed){
            continue;
        }
        
        if (dt > 1000*get_conf()->refresh_period /* in µs */){
            LED_Set(1);
            Start_timer();
            generate_event(event_counter);

            if (evt_buff_n == EVT_BUFF_DEPTH){
                if (full_display == 1){
                    full_display = 0;
                    print_first_line("Rh-T running !");
                }
                else{
                    full_display = 1;
                    print_first_line("Buffer full !");
                }
            }
            else{
                print_first_line("Rh-T running !");
            }
            
            TM_DisplayTime(2);
            event_counter++;
            LED_Set(0);
        }
        
        run_serial();
        if (full_display && evt_buff_n < EVT_BUFF_DEPTH){
            full_display = 0;
            print_second_line("");
        }
        
    }
}

