/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/
#ifndef PT1000_H
#define PT1000_H

    #define ADG728_ADDR (0b1001100)
    
    #define PT_AVG  (128)
    
    void PT_Init();//performs all initialisation
    void PT_Update_Next();//update one PT, following the cycle in MuXseq and duplicated to external ADG728 mux
    void PT_Update_All();//update all 8 PTs
    
    float PT_array[8];//global array to store temperatures
#endif
/* [] END OF FILE */
