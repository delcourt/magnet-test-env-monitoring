#include "lemo_counter.h"
#include "project.h"

void lemo_init(){
    Counter_Lemo_0_Start();
    Counter_Lemo_1_Start();
    Counter_Lemo_2_Start();
    Counter_Lemo_3_Start();
    Comp_1_Start();
    Comp_2_Start();
    //Comp_3_Start();
//    Comp_4_Start();
}

int lemo_get_counts(int lemo_id){
    if (lemo_id == 0)
        return Counter_Lemo_0_ReadCounter();
    else if (lemo_id == 1)
        return Counter_Lemo_1_ReadCounter();
    else if (lemo_id == 2)
        return Counter_Lemo_2_ReadCounter();
    else if (lemo_id == 3)
            return Counter_Lemo_3_ReadCounter();
}

void lemo_reset(){
    Counter_Lemo_0_WriteCounter(0);
    Counter_Lemo_1_WriteCounter(0);
    Counter_Lemo_2_WriteCounter(0);
    Counter_Lemo_3_WriteCounter(0);
}