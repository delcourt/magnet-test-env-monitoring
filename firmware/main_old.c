/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/
#include "project.h"
#include "TCA9534.h"
#include <time.h>

int main_old(void)
{
    CyGlobalIntEnable; /* Enable global interrupts. */
    char tmp;
    
    
    //char I2C_buff[16];
    PWM_display_Start();
    PWM_display_WriteCompare(999);
    I2C_rtc_Start();
    
    /*I2C_LCD_Start();
    //LCD init
    //from S7032i doc
    I2C_LCD_WriteControl(0x38);//2lines, 4bit
    CyDelayUs(30);
    I2C_LCD_WriteControl(0x38);//twice
    CyDelayUs(30);
    I2C_LCD_WriteControl(0x14);//internal oscillator freq
    CyDelayUs(30);
    I2C_LCD_WriteControl(0x7f);//contrast
    CyDelayUs(30);
    I2C_LCD_WriteControl(0x50);//contrast/power
    CyDelayUs(30);
    I2C_LCD_WriteControl(0x6c);//follower control
    CyDelay(200);
    I2C_LCD_WriteControl(0x0c);//display control
    CyDelayUs(30);
    
    I2C_LCD_Init();
    I2C_LCD_DisplayOn();
    // I2C_LCD_WriteControl(I2C_LCD_DISPLAY_2_LINES_5x10 );
    I2C_LCD_PrintString("YGA test Rh-T");
    */
    /* Place your initialization/startup code here (e.g. MyInst_Start()) */

    IO_Init();
   
    
   /* O_Set(O_D3);
    O_Set(O_D4);
    O_Set(O_EN0);
    O_Set(O_EN1);
    
    LED_Set(1);
    LED_Set(2);
    LED_Set(3);
    MUX_Set(1);
    MUX_Set(2);
    MUX_Set(3);*/
    
    /*TODO
    IO exp mostly done
    LCD
    RTC pending
    FLASH
    NIM done
    USB
    FSM
    
    I2C mux config
    ACC
    RH-T read
    PT1000
    ANALOG MUX config
    
    
    */
    
    char MUX0_buff[2];
    
   // char HIH_buff[3];
    
    MUX_Set(1);
    CyDelay(100);
    
    I2C_1_Start();//mux 0
    
    #define PCA9544_ADDR    (0b1110000) //MUX0
    #define HIH_ADDR    (0x27)
    
    PT_Init();
    
    /*I2C_1_MasterClearStatus();
    MUX0_buff[0]=0b110;//activate all
    //I2C_buff[1]=mask&~TCA9534_DEFCONF;//ENx are active low, keeping them off. Also, leds off
    I2C_1_MasterWriteBuf(PCA9544_ADDR, MUX0_buff, 1, I2C_1_MODE_COMPLETE_XFER);
    while(!(I2C_1_MasterStatus()&I2C_1_MSTAT_WR_CMPLT));
    */
    
    
   /*  I2C_1_MasterClearStatus();
    MUX0_buff[0]=0b111;//activate all
    //I2C_buff[1]=mask&~TCA9534_DEFCONF;//ENx are active low, keeping them off. Also, leds off
    I2C_1_MasterWriteBuf(PCA9544_ADDR, MUX0_buff, 1, I2C_1_MODE_COMPLETE_XFER);
    while(!(I2C_1_MasterStatus()&I2C_1_MSTAT_WR_CMPLT));*/
    
   
   
   // adc_pt_mes=ADC_PT_GetResult16();//measure with no current flowing in R
   // ADC_PT_SetOffset(adc_pt_mes);//set offset
    
    while(1)
    {
        led_kprog_Write(!led_kprog_Read());
        //PT_Update_Next();
        PT_Update_All();
        
    volatile char status=I2C_rtc_MasterStatus();
    
    if (I2C_rtc_MSTAT_RD_CMPLT& status) 
        asm("nop");
    if (I2C_rtc_MSTAT_WR_CMPLT&status)
        asm("nop");
    if (I2C_rtc_MSTAT_XFER_INP&status)
        asm("nop");
    if (I2C_rtc_MSTAT_XFER_HALT&status)
        asm("nop");
    if (I2C_rtc_MSTAT_ERR_SHORT_XFER&status)
        asm("nop");
    if (I2C_rtc_MSTAT_ERR_ADDR_NAK&status)
        asm("nop");
    if (I2C_rtc_MSTAT_ERR_ARB_LOST&status)
        asm("nop");
    if (I2C_rtc_MSTAT_ERR_XFER&status)
        asm("nop");
    
        
//        CyDelay(250);
        
        
        
        tmp<<=1;
        tmp&=0xf;
        tmp|=!tmp;
        
        //useless
        //adg_sel|=!adg_sel;
        
        //tmp=LEMO_Read();
        
        /*LED_set(tmp&3);*/
        
        //tmp=SW_get();
       // LED_Set(tmp);
      //  MUX_Set((tmp>>2)&3);
    /*    
         //write to 0x27 to initiate conversion
    I2C_1_MasterClearStatus();
    I2C_1_MasterWriteBuf(HIH_ADDR, MUX0_buff, 0, I2C_1_MODE_COMPLETE_XFER);
    while(!(I2C_1_MasterStatus()&I2C_1_MSTAT_WR_CMPLT));
    
    
    //wait 50ms
    CyDelay(70);
    
    //get 4 bytes from 0x27
    char HIH_buff[5];
    I2C_1_MasterClearStatus();
    I2C_1_MasterReadBuf(HIH_ADDR, HIH_buff, 4, I2C_1_MODE_COMPLETE_XFER);
     while(!(I2C_1_MasterStatus()&I2C_1_MSTAT_RD_CMPLT));
    //change mux
   


    
    volatile float HIH_rh, HIH_tmp;
    
    HIH_rh=100*((HIH_buff[0]&0x3f)<<8)+HIH_buff[1];
    HIH_rh/=(1<<14)-2;
    HIH_tmp=(((HIH_buff[2]<<8)+HIH_buff[3])>>2);

    HIH_tmp/=(1<<14)-2;
    HIH_tmp=HIH_tmp*165-40;
        
        */
        
     asm("nop");    
        /* Place your application code here. */
    }
}

/* [] END OF FILE */
