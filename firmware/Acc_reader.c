#include "Acc_reader.h"
#include "therm_reader.h"
#include "TCA9534.h"
#include <stdio.h>
#include "utils.h"

#define ACC_ADD 0x15

int read_acc(int channel, uint8_t reg, uint8_t * buff, int n_bytes){
    int status = i2c_writer(channel,ACC_ADD,&reg,1);
    status    &= i2c_reader(channel,ACC_ADD,buff,n_bytes);
    if (status == 0){
        LED_Set(2);
    }
    return status;
}

int write_acc(int channel, uint8_t reg, uint8_t * buff, int n_bytes){
    uint8_t data[n_bytes + 1];
    data[0] = reg;
    for (int i = 0; i< n_bytes; i++){
        data[i+1] = buff[i];
    }
    //int status  = i2c_writer(channel,ACC_ADD,&reg,1);
    int status = i2c_writer(channel,ACC_ADD,data,n_bytes);
    if (status == 0){
        LED_Set(2);
    }
    return status;
}

int write_acc_1b(int channel, uint8_t reg, uint8_t data){
    uint8_t yyy = data;
    int status  = i2c_reader(channel,ACC_ADD,&reg,0);
        status  &= i2c_writer(channel,ACC_ADD,&yyy,1);
    if (status == 0){
        LED_Set(2);
    }
    return status;
}

int test_acc(int channel){
    uint8_t add = 0;
    return (read_acc(channel,0x00,&add,1) && add==0x90);
}

int configure_acc(int channel){
    set_normal_power_mode();
    //write_acc_1b(channel,0x19,0x02);
    //CyDelay(2);
    //write_acc_1b(channel,0x7C,0x01);
    //write_acc_1b(channel,0x1A,0x38);
    //write_acc_1b(channel,0x1B,0x04);
    //write_acc_1b(channel,0x1F,0x80);
    //write_acc_1b(channel,0x21,0x80);
    return 0;    
}

int set_normal_power_mode(int channel){
    
   //   writeByte(BMA400_ADDRESS,BMA400_ACC_CONFIG0, 0x80 | OSR << 5 | power_Mode);
   //   delay(2); // wait 1.5 ms
   //   writeByte(BMA400_ADDRESS,BMA400_ACC_CONFIG1, Ascale << 6 | OSR << 4 | SR); // set full-scale range, oversampling and sample rate
   //   writeByte(BMA400_ADDRESS,BMA400_ACC_CONFIG2, acc_filter << 2);             // set accel filter 
    
    
   //uint8_t POWER_MODE = 1;  //SLEEP = 0; LOW = 1; NORMAL = 2
   char LCD_buf[18];
   uint8_t conf = 0;
   int status = read_acc(channel,0x03,&conf,1); // getting config
   snprintf(LCD_buf, 17,"Conf was : %X" ,conf);
   print_first_line(LCD_buf);
   
   if (status == 0){
    print_second_line("ERROR");
    CyDelay(50000);
   }
    CyDelay(1000);
//   conf &= 0xF6;
//   conf |= 2;
   conf = 0xE2;

   status = write_acc_1b(channel,0x19,conf);
   snprintf(LCD_buf, 17,"Sent : %X" ,conf);
   print_first_line(LCD_buf);
   if (status == 0){
    print_second_line("ERROR");
    CyDelay(50000);
   }
   CyDelay(1000);
   status = read_acc(channel,0x03,&conf,1);
   snprintf(LCD_buf, 17,"Received : %X" ,conf);
   print_first_line(LCD_buf);
   if (status == 0){
    print_second_line("ERROR");
    CyDelay(50000);
   }
//   read_acc(channel,0x03,&conf,1); // getting config
   //snprintf(LCD_buf, 17,"Received: %X" ,conf);
   //print_first_line(LCD_buf);
   
   
   //snprintf(LCD_buf, 17,"Pow now : %i",(conf>>1)&0x03);
   //print_second_line(LCD_buf);

   //return write_acc(channel,
   return 0;
}

void print_accel(int channel){
   uint8_t data[6];
   read_acc(channel,0x08,data,6);
   int accel_X = data[0] + 256*data[1];
   if (accel_X > 2047){
    accel_X = -accel_X;
   }
   int accel_Y = data[2] + 256*data[3];
   if (accel_Y > 2047){
    accel_Y = -accel_Y;
   }
   int accel_Z = data[4] + 256*data[5];
   if (accel_Z > 2047){
    accel_Z = -accel_Z;
   }
   char LCD_buf[18];
   print_first_line("Accel : ");
   snprintf(LCD_buf, 17,"%X %X %X",accel_X,accel_Y,accel_Z);
   print_second_line(LCD_buf);
}

void my_tester(){
    if (test_acc(1)){
        asm("nop");
    }
    if (set_normal_power_mode(1)){
        asm("nop");
    }
}

void acc_test_loop(){
    print_first_line("Testing accelerometer");
    CyDelay(1000);
    if (!test_acc(1)){
        print_first_line("NO ACK FROM ACC");
        CyDelay(5000);
    }
    else {
        print_second_line("acc says hi !");
    }
    configure_acc(1);
    Start_timer();
    while(1){
        uint32_t dt = Get_timer();
        if (dt > 1000000 /* in µs */){
            //print_accel(1);
            Start_timer();
        }
    }
    
}