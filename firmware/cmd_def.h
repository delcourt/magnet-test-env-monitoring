#pragma once

#define CMD_SEND_DATA    0xAA
#define CMD_SYNC         0xA1
#define CMD_READ_FLASH   0xA2
#define CMD_BUFFER_DEPTH 0xA3
#define CMD_SCAN_I2C     0xA4
#define CMD_GET_N_FLASH_BLOCKS 0xA5
#define CMD_SEND_CONF    0xA6
#define CMD_GET_CONF     0xA7
#define CMD_GET_CONF_SIZE 0xA8

#define CMD_ACK         0xFF
#define CMD_ERROR       0x00


#define CMD_TIMEOUT         5       // time (in sec) before timeout.