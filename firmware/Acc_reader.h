#pragma once
#include "project.h"

int test_acc(int channel);
int set_normal_power_mode();

int configure_acc();

void read_out_acc();

void acc_test_loop();