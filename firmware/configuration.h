#pragma once
#include <stdio.h>

struct conf{
    int32_t timezone;
    int32_t refresh_period; // Refresh period (in seconds)
    int32_t flash_prescale; // Flash refresh period (in units of base refresh rate)
    int32_t dcs_thresh;     // 4 bits maj, 10 bits H_up, 10 bits H_down, 8 bits mask
    int32_t lemo_routing;   // 2 LSB = V_set signal
    int32_t misc;           // MSB to LSB : UNUSED
};

struct conf * get_conf();

void    set_default_conf();          // Set default config values
int     get_conf_8b (uint8_t * buf); // Utils for serial driver
void    load_conf_8b(uint8_t * buf); 


void   reload_conf();
void   write_conf();
