#include "configuration.h"
#include "ds3232.h"

#define CONF_OFFSET 2 //offset of configuration in RTC flash. First two words are reserved for flash management

struct conf config_;
int conf_valid = 0;

struct conf * get_conf(){
    if (conf_valid == 0){
        reload_conf();
        conf_valid = 1;
    }
    return &config_;
}

void store_word(int32_t word, uint8_t * buf, int * pos){
    buf[*pos]   = (uint8_t) ((word>>24) & 0xFF);
    buf[*pos+1] = (uint8_t) ((word>>16) & 0xFF);
    buf[*pos+2] = (uint8_t) ((word>>8)  & 0xFF);
    buf[*pos+3] = (uint8_t) ((word)     & 0xFF);
    *pos+=4;
}

int32_t read_word (uint8_t * buf, int * pos){
    int32_t word = (int32_t)((buf[*pos]<<24) + (buf[*pos+1]<<16) + (buf[*pos+2]<<8) + buf[*pos]);
    *pos +=4;
    return word;
}


int get_conf_8b (uint8_t * buf){
    int pos = 0;
    store_word(config_.timezone         ,buf,&pos);
    store_word(config_.refresh_period   ,buf,&pos);
    store_word(config_.flash_prescale   ,buf,&pos);
    store_word(config_.dcs_thresh       ,buf,&pos);
    store_word(config_.misc             ,buf,&pos);
    return pos;
}

void    load_conf_8b(uint8_t * buf){
    int pos = 0;
    config_.timezone         =  read_word(buf,&pos);
    config_.refresh_period   =  read_word(buf,&pos);
    config_.flash_prescale   =  read_word(buf,&pos);
    config_.dcs_thresh       =  read_word(buf,&pos);
    config_.misc             =  read_word(buf,&pos);
}

void   reload_conf(){
    config_.timezone         =  RTC_ReadSRAM(0 + CONF_OFFSET);
    config_.refresh_period   =  RTC_ReadSRAM(1 + CONF_OFFSET);
    config_.flash_prescale   =  RTC_ReadSRAM(2 + CONF_OFFSET);
    config_.dcs_thresh       =  RTC_ReadSRAM(3 + CONF_OFFSET);
    config_.misc             =  RTC_ReadSRAM(4 + CONF_OFFSET);
}

void   write_conf(){
    RTC_WriteSRAM(0 + CONF_OFFSET, config_.timezone         );
    RTC_WriteSRAM(1 + CONF_OFFSET, config_.refresh_period   );
    RTC_WriteSRAM(2 + CONF_OFFSET, config_.flash_prescale   );
    RTC_WriteSRAM(3 + CONF_OFFSET, config_.dcs_thresh       );
    RTC_WriteSRAM(4 + CONF_OFFSET, config_.misc             );
}

void  set_default_conf(){
    config_.timezone         =  7200;
    config_.refresh_period   =  1000;
    config_.flash_prescale   =  10;
    config_.dcs_thresh       =  (3<<28) + (100 << 18) + (100<< 8) + 15;
    config_.lemo_routing     =  3;
    config_.misc             =  0x00000000;
    conf_valid = 1;
    write_conf();

}
