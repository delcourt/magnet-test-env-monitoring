/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/

#include "project.h"
#include "LCD.h"


void LCD_Position(uint8 x, uint8 y) {
    uint8 tmp;
    //x= position on the line, 0x0 to 0x27, starts at 0
    //y= line number, start also at 0
    tmp=0x80+x+0x40*y;
    LCD_Control(tmp);
    
}

void LCD_Control(uint8 control) {
    uint8_t LCD_buff[65];
    LCD_buff[0]=0b00000000;
    LCD_buff[1]=control;
    volatile char status;
    I2C_rtc_MasterWriteBuf(LCD_ADDR, LCD_buff, 2, I2C_rtc_MODE_COMPLETE_XFER);
    while(!(status=I2C_rtc_MasterStatus()&I2C_rtc_MSTAT_WR_CMPLT));
    if (I2C_rtc_MSTAT_RD_CMPLT& status) 
        asm("nop");
    if (I2C_rtc_MSTAT_WR_CMPLT&status)
        asm("nop");
    if (I2C_rtc_MSTAT_XFER_INP&status)
        asm("nop");
    if (I2C_rtc_MSTAT_XFER_HALT&status)
        asm("nop");
    if (I2C_rtc_MSTAT_ERR_SHORT_XFER&status)
        asm("nop");
    if (I2C_rtc_MSTAT_ERR_ADDR_NAK&status)
        asm("nop");
    if (I2C_rtc_MSTAT_ERR_ARB_LOST&status)
        asm("nop");
    if (I2C_rtc_MSTAT_ERR_XFER&status)
        asm("nop");
}


void LCD_Init() {
 CyDelay(40);
    I2C_LCD_Start();
    
    //most copmments in this section are not corresponding toi code because of zillions trials and errors
    
    
    //LCD init
    //from S7032i doc
    //I2C_LCD_WriteControl(0x38);//2lines, 4bit
    /*LCD_buff[0]=0b01000000;
    LCD_buff[1]=0x38;
    
    I2C_rtc_MasterWriteBuf(LCD_ADDR, LCD_buff, 2, I2C_rtc_MODE_COMPLETE_XFER);
    while(!(status=I2C_rtc_MasterStatus()&I2C_rtc_MSTAT_WR_CMPLT));
    */
    LCD_Control(0x39); // 0x38
    
    CyDelayUs(30);
    LCD_Control(0x39);//twice, almost. This once has to be 0x39 esle display does not work
    CyDelayUs(30);
    LCD_Control(0x14);//internal oscillator freq
    CyDelayUs(30);
    LCD_Control(0x70+0xf);//contrast   0x70+0xf
    CyDelayUs(30);
    LCD_Control(0x58+3);//contrast/power 0x58 + 3
    CyDelayUs(30);
    //LCD_Control(0x68);//follower control
    CyDelayUs(30);
    LCD_Control(0x6F);//display control
    
    CyDelay(200);
    LCD_Control(0x0);//display control
    CyDelayUs(30);
    LCD_Control(0x1);//display control
    CyDelayUs(30);
    LCD_Control(0x6);//display control
    
    
    CyDelayUs(30);
    //I2C_LCD_Init();
    I2C_LCD_DisplayOn();
    I2C_LCD_ClearDisplay();
 //   I2C_LCD_Position(1,1);//this one does not work correctly

    I2C_LCD_WriteControl(I2C_LCD_RESET_CURSOR_POSITION);   
    
}


/* [] END OF FILE */
