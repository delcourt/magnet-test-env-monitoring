#include "serial_driver.h"
#include "therm_reader.h"
#include "utils.h"
#include "cmd_def.h"
#include "ds3232.h" //Time management
#include "configuration.h"

enum action{ NOTHING, SEND_EVENT, READ_FLASH, SEND_FLASH, SYNC, SEND_CONF, GET_CONF};
enum action TODO = NOTHING;

uint32_t last_cmd_timestamp = 0;
uint16_t flash_block_id = 0;
uint16_t flash_block_pos = 0;
uint8_t  flash_data[BLOCK_SIZE];
uint8_t  conf_size = sizeof(struct conf);
int byte_to_send = 0;

void send_acknowledge(){
    uint8_t ack = CMD_ACK;
    USBFS_1_PutData(&ack,1);
}

void send_error(){
    uint8_t ack = CMD_ACK;
    USBFS_1_PutData(&ack,1);
}

void send_data(uint8_t * dd, int n_bytes){
    uint8_t buff[n_bytes+1];
    buff[0] = CMD_ACK;
    for (int i = 0; i<n_bytes; i++)
        buff[i+1] = dd[i];
    USBFS_1_PutData(buff,n_bytes+1);
}

int get_byte(){
    if (0u != USBFS_1_GetConfiguration()){
           if (0u != USBFS_1_IsConfigurationChanged())
            {
                if (0u != USBFS_1_GetConfiguration())
                {
                    USBFS_1_CDC_Init();
                }
            }
            
            if (0u != USBFS_1_DataIsReady())
            {   
                uint8_t x;
                USBFS_1_GetData(&x,1);
                return x;
            }
            else{
                return 0;
            }
            
        }
    return 0;
}



void send_event(){
    if (evt_buff_n == 0){
        send_error();
    }
    else {
        int index = (EVT_BUFF_DEPTH + evt_buff_offset - evt_buff_n)%EVT_BUFF_DEPTH;
        evt_buff_n --;
        evt_buff_overflow = 0;
    
        uint8_t * buff = evt_buff[index];
        USBFS_1_PutData(buff,EVT_SIZE);
    }
}

void send_n_flash_blocks(){
    uint16_t n_blocks = flash_get_n_block();
    uint8_t  buff [3];
    buff[0] = CMD_ACK;
    buff[1] = n_blocks >>8;
    buff[2] = n_blocks & 0xFF;
    USBFS_1_PutData(buff,3);
}

void run_serial(){
    if (TODO == NOTHING && USBFS_1_CDCIsReady()){
        int cmd = get_byte();
        if (cmd != 0){
            last_cmd_timestamp = TM_GetUnixTime();   
        }
        if (cmd == CMD_SEND_DATA){    
            TODO = SEND_EVENT;
            //print_first_line("Sending event !");
            send_acknowledge();
        }
        else if (cmd == CMD_SYNC){
            //print_first_line("Syncinc clock !");
            send_acknowledge();
            TODO = SYNC;
        }
        else if (cmd == CMD_READ_FLASH){
            print_first_line("Reading flash !");
            send_acknowledge();
            TODO = READ_FLASH;
        }
        else if (cmd == CMD_BUFFER_DEPTH){
            //print_first_line("Sending buffer status");
            send_data(&evt_buff_n,1);
            TODO = NOTHING;
        }
        else if (cmd == CMD_SCAN_I2C){
            send_acknowledge();
            print_second_line("Rescanning I2C...");   
            setup_HIH_I2C();
            TODO = NOTHING;
        }
        else if (cmd == CMD_GET_N_FLASH_BLOCKS){
            send_n_flash_blocks();
            TODO = NOTHING;
        }
        else if (cmd == CMD_SEND_CONF){
            send_acknowledge();
            TODO = SEND_CONF;   
        }
        else if (cmd == CMD_GET_CONF){
            send_acknowledge();
            TODO = GET_CONF;   
        }
        else if (cmd == CMD_GET_CONF_SIZE){
            send_data(&conf_size,1);
            TODO = NOTHING;
        }
        else if (cmd != 0){
            snprintf(LCD_buf, 17,"Bad cmd : %2X",cmd);
            print_first_line(LCD_buf);
            //send_error();
            USBFS_1_CDC_Init();
            TODO = NOTHING;
        }    
    }
    else{
        if ( TM_GetUnixTime() - last_cmd_timestamp >  CMD_TIMEOUT && last_cmd_timestamp != 0){
            print_first_line("Serial timeout");
            //send_error();
            TODO = NOTHING;
        }
    }
    
    if (TODO == SEND_EVENT && evt_buff_n > 0 && USBFS_1_CDCIsReady()){
        send_event();
        TODO = NOTHING;
    }
    else if (TODO == SYNC && USBFS_1_GetCount() == 8){
            uint64_t tt = 0;
            uint8_t  bb[8];
            USBFS_1_GetData(bb,8);
            tt = (bb[0] <<24 )+ (bb[1] <<16) + (bb[2] <<8) + bb[3];
            TM_SetUnixTime(tt);
            int timezone = (int)((bb[4] <<24 )+ (bb[5] <<16) + (bb[6] <<8) + bb[7]);
            TM_SetTimezone(timezone);
            TODO = NOTHING;
    }
    else if (TODO == READ_FLASH && USBFS_1_GetCount() == 2){
        uint8_t dd[2];
        USBFS_1_GetData(dd,2);
        flash_block_id = (dd[0]<<8)+dd[1];
        
        flash_get_block(flash_block_id,flash_data);
        flash_block_pos = 0;
        TODO = SEND_FLASH;
    }
    else if (TODO == SEND_FLASH && USBFS_1_CDCIsReady()){       
        USBFS_1_PutData(&(flash_data[flash_block_pos]),64);
        flash_block_pos += 64;
        if (flash_block_pos >= BLOCK_SIZE){
            TODO = NOTHING;
            print_second_line("DONE !");
        }
    }
   else if (TODO == GET_CONF && USBFS_1_GetCount() == conf_size){
        uint8_t dd[conf_size];
        USBFS_1_GetData(dd,conf_size);
        load_conf_8b(dd);    
        TODO = NOTHING;
   }
   else if (TODO == SEND_CONF && USBFS_1_CDCIsReady()){
        uint8_t buff[conf_size];
        USBFS_1_PutData(buff, conf_size);
        TODO = NOTHING;
   }
}


