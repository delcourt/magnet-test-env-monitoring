/* ========================================
 *
 * Copyright YOUR COMPANY, THE YEAR
 * All Rights Reserved
 * UNPUBLISHED, LICENSED SOFTWARE.
 *
 * CONFIDENTIAL AND PROPRIETARY INFORMATION
 * WHICH IS THE PROPERTY OF your company.
 *
 * ========================================
*/

/* [] END OF FILE */
#include "project.h"
#include "TCA9534.h"

uint8_t I2C_buff[16];

 //init IO exp TCA9534
void IO_Init() {//write magic values to registers for proper operation
    
    I2C_rtc_MasterClearStatus();
    //writing direction register
    I2C_buff[0]=TCA9534_CONF;
    I2C_buff[1]=TCA9534_DEFCONF;
    
    I2C_rtc_MasterWriteBuf(TCA9534_ADDR, I2C_buff, 2, I2C_rtc_MODE_COMPLETE_XFER);
    while(!(I2C_rtc_MasterStatus()&I2C_rtc_MSTAT_WR_CMPLT));
    
    
    //writing output values
    I2C_rtc_MasterClearStatus();
    I2C_buff[0]=TCA9534_OPORT;
    I2C_buff[1]=/*O_D4|O_D3|*/O_EN0|O_EN1;//ENx are active low, keeping them off. Also, leds off
    
    I2C_rtc_MasterWriteBuf(TCA9534_ADDR, I2C_buff, 2, I2C_rtc_MODE_COMPLETE_XFER);
    while(!(I2C_rtc_MasterStatus()&I2C_rtc_MSTAT_WR_CMPLT));
    
    //set invert for switches
    I2C_rtc_MasterClearStatus();
    I2C_buff[0]=TCA9534_POLINV;
    I2C_buff[1]=I_SW1|I_SW2;//TODO move magic value to .h
    
    I2C_rtc_MasterWriteBuf(TCA9534_ADDR, I2C_buff, 2, I2C_rtc_MODE_COMPLETE_XFER);
    while(!(I2C_rtc_MasterStatus()&I2C_rtc_MSTAT_WR_CMPLT));
    
    
}

//set IO

char O_Set(char mask) {
    I2C_rtc_MasterClearStatus();
    I2C_buff[0]=TCA9534_OPORT;
    I2C_buff[1]=mask&~TCA9534_DEFCONF;//ENx are active low, keeping them off. Also, leds off
    I2C_rtc_MasterWriteBuf(TCA9534_ADDR, I2C_buff, 2, I2C_rtc_MODE_COMPLETE_XFER);
    while(!(I2C_rtc_MasterStatus()&I2C_rtc_MSTAT_WR_CMPLT));
    //todo return -1/255 if bits are set ouside allowed bits
}

char REG_Get(char reg){
    uint8_t value;
    I2C_rtc_MasterClearStatus();
    I2C_buff[0]=reg&0x3;//TODO add error reporting
    I2C_rtc_MasterWriteBuf(TCA9534_ADDR, I2C_buff, 1, I2C_rtc_MODE_NO_STOP); //write reg, no stop
    while(!(I2C_rtc_MasterStatus()&I2C_rtc_MSTAT_WR_CMPLT));
    
     I2C_rtc_MasterReadBuf(TCA9534_ADDR, &value, 1, I2C_rtc_MODE_REPEAT_START);
     while(!(I2C_rtc_MasterStatus()&I2C_rtc_MSTAT_RD_CMPLT));
    
    return value;
    
}


char SW_Get(){
    uint8_t value;
    I2C_rtc_MasterClearStatus();
    I2C_buff[0]=TCA9534_IPORT;
    I2C_rtc_MasterWriteBuf(TCA9534_ADDR, I2C_buff, 1, I2C_rtc_MODE_NO_STOP); //write reg, no stop
    while(!(I2C_rtc_MasterStatus()&I2C_rtc_MSTAT_WR_CMPLT));
    
     I2C_rtc_MasterReadBuf(TCA9534_ADDR, &value, 1, I2C_rtc_MODE_REPEAT_START);
     while(!(I2C_rtc_MasterStatus()&I2C_rtc_MSTAT_RD_CMPLT));
    
    value>>=6;
    value=3&((value>>1)|(value<<1));
    return value;
    
}

char I_Get(){
    uint8_t value;
    I2C_rtc_MasterClearStatus();
    I2C_buff[0]=TCA9534_IPORT;
    I2C_rtc_MasterWriteBuf(TCA9534_ADDR, I2C_buff, 1, I2C_rtc_MODE_NO_STOP); //write reg, no stop
    while(!(I2C_rtc_MasterStatus()&I2C_rtc_MSTAT_WR_CMPLT));
    
     I2C_rtc_MasterReadBuf(TCA9534_ADDR, &value, 1, I2C_rtc_MODE_REPEAT_START);
     while(!(I2C_rtc_MasterStatus()&I2C_rtc_MSTAT_RD_CMPLT));
    
 //   value>>=6;
   // value=3&((value>>1)|(value<<1));
    return value;//raw value from register
    
}


void MUX_Set(char value) {//0b EN1 EN0
    //TODO read/modify/write
  char reg;
   char tmp;
  reg=REG_Get(TCA9534_OPORT); //get current value
  tmp=reg|(O_EN0|O_EN1); //clear relevant bits (inverted logic)
  if ( value == (value&0x3) ) { //pass right aligned value
    tmp&=~((value&1)*O_EN0);
    tmp&=~(!!(value&2)*O_EN1);
    O_Set(tmp); //write modified value, inverted logic: low=ON
    }
 else { O_Set(tmp&~(value&(O_EN0|O_EN1)));} //or mask to activate

}

void LED_Set(char value) {// 0b D4 D3
  //TODO read/modify/write
  char reg;
  char tmp;
  reg=REG_Get(TCA9534_OPORT); //get current value
  tmp=reg&~(O_D3|O_D4); //clear relevant bits
 if (value == (value&0x3)) { //pass right aligned value
    tmp|=((value&1)*O_D3);
    tmp|=(!!(value&2)*O_D4);
    O_Set(tmp); 
}
 else { O_Set(tmp|value&(O_D3|O_D4));} //or mask to activate 
//FIXME !!!
    
    
}


